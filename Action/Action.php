<?php

namespace Declic3000\Pelican\Action;


use Declic3000\Pelican\Service\Chargeur;
use Declic3000\Pelican\Service\LogMachine;
use Declic3000\Pelican\Service\Requete;
use Declic3000\Pelican\Service\Robotinit;
use Declic3000\Pelican\Service\Sac;
use Declic3000\Pelican\Service\Suc;
use Doctrine\ORM\EntityManagerInterface;

class Action
{

    protected $em;
    protected $requete;
    protected $sac;
    protected $db;
    protected $suc;
    protected $log;
    protected $objet = '';
    protected $chargeur;
    protected $robotinit;

    /**
     * {@inheritdoc}
     */
    public function __construct(Requete $requete, EntityManagerInterface $em, Sac $sac, Suc $suc, LogMachine $log)
    {
        $this->sac = $sac;
        $this->suc = $suc;
        $this->requete = $requete;
        $this->em = $em;
        $this->db = $em->getConnection();
        $this->log = $log;
        $this->chargeur = new Chargeur($em);
        $this->robotinit = new Robotinit($em, $sac);

    }

    function creation_modification($data)
    {
        $objet_data = null;
        $cle = $this->sac->descr($this->objet . '.cle_sql');
        $modification = false;
        $ob = null;
        if (isset($data[$this->objet])) {
            $ob = $data[$this->objet];
            $modification = true;
            unset($data[$cle]);
        } elseif (isset($data[$cle]) and $data[$cle] > 0) {
            $ob = $this->chargeur->charger_objet($this->objet, $data[$cle]);
            if ($ob) {
                $modification = true;
            }
            unset($data[$cle]);
        }
        if (!$modification || empty($ob)) {
            $nom_classe_entity = '\\App\\Entity\\' . camelize($this->objet);
            $ob = new $nom_classe_entity();
        }
        $data = $this->creation_modification_cplt($data, $objet_data, $modification);

        foreach ($data as $cle => $valeur) {
            if ($valeur) {
                $objet_nom = substr((string)$cle, 3);
                $descr_objet = $this->sac->descr($objet_nom);
                if (!empty($descr_objet)) {
                    if (str_starts_with((string)$cle, 'id_')) {
                        $objet = $this->chargeur->charger_objet($objet_nom, $valeur);
                        if ($objet) {
                            unset($data[$cle]);
                            $data[camelize2($objet_nom) . ""] = $objet;
                        }
                    }
                }
            }
        }
        $ob->fromArray($data);
        $this->em->persist($ob);
        $this->em->flush();
        return $ob;

    }

    function creation_modification_cplt($data, $objet_data, $modification = false)
    {
        return $data;
    }

    function debusque_objet($form, $champs, $objet, $critere = null)
    {
        $val = $form->get($champs)->getData();
        if (!empty($val)) {
            if (((int)$val) > 0) {
                return $this->chargeur->charger_objet($objet, (int)$val);
            } else {
                preg_match("`- (.?[0-9]*)$`ui", (string)$val, $matches);
                if (isset($matches[1])) {
                    if ($critere) {
                        return $this->chargeur->charger_objet_one_by($objet, ['code' => $matches[1]]);
                    } else {
                        return $this->chargeur->charger_objet($objet, $matches[1]);
                    }
                }
            }
        }
        return null;
    }
}
