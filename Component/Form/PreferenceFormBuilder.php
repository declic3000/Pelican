<?php

namespace Declic3000\Pelican\Component\Form;

use App\Entity\Config;
use Declic3000\Pelican\Service\Ged;
use Declic3000\Pelican\Service\Requete;
use Declic3000\Pelican\Service\Sac;
use Declic3000\Pelican\Service\Suc;
use Declic3000\Pelican\Service\Uploader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\ColorType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Contracts\Translation\TranslatorInterface;

class PreferenceFormBuilder
{
    protected $requete;
    protected $router;
    protected $sac;
    protected $suc;
    protected $formbuilder;
    protected $em;
    protected $translator;

    public function __construct(FormFactory $formbuilder, TranslatorInterface $translator, Requete $requete, Sac $sac, Suc $suc, UrlGeneratorInterface $router, EntityManagerInterface $em)
    {
        $this->formbuilder = $formbuilder;
        $this->requete = $requete;
        $this->sac = $sac;
        $this->suc = $suc;
        $this->router = $router;
        $this->em = $em;
        $this->translator = $translator;
    }


    // Transforme une entrée de préf

    function construire_form_preference($pref, $niveau = 1, $args = [], $nom_formulaire = '', $retourne_builder = false, $options = [])
    {
        $data = $this->suc->pref($pref);
        $tab_entree = $this->sac->get('preference_meta');
        $cle = $pref;
        if (!strpos((string)$pref, '.') === false) {
            $pref0 = explode('.', (string)$pref);
            $cle = $pref0[0];
            if (is_array($pref0) && count($pref0) > 1) {
                array_shift($pref0);
                $tab_entree = tableauChemin($tab_entree[$cle]['variables'], implode('.', $pref0));
            }
        } else {

            $tab_entree = $tab_entree[$cle]['variables'];
        }

        if (!$this->suc->get('entite_multi')) {
            unset($tab_entree['tous_les_entites']);
            unset($tab_entree['id_entite']);
        }

        $args['pref'] = str_replace('.', '|', $pref);

        if (empty($data)) {
            $data = [];
        }
        if ($retourne_builder) {
            return $this->pretransforme_en_formulaire($pref, $tab_entree, $data, $niveau, $args, $nom_formulaire, $options);
        } else {
            return $this->transforme_en_formulaire($pref, $tab_entree, $data, 'preference', $niveau, $args, $nom_formulaire, $options);
        }


    }

    function pretransforme_en_formulaire($nom, $entree, $data, $niveau_max = 1, $args = [], $nom_formulaire = '', $options = [])
    {
        if (!is_array($entree)) {
            $entree = [$nom => $entree];
            $data = [$nom => $data];
        }

        $data = $this->transforme_data_suivant_entree($nom, $data, $entree);

        if ($nom_formulaire == '') {
            $nom_formulaire = 'cfpf_' . str_replace('.', '_point_', $nom);
        } else {
            $nom_formulaire = str_replace('.', '_point_', $nom);
        }

        if ($niveau_max > 1)
            $args['niveau'] = $niveau_max;
        $page = $this->sac->get('page');
        $args += [
            'pref' => $nom,
            'redirect' => $this->requete->get('redirect')
        ];
        $options = array_merge($options, [
            'required' => false,
            'action' => $this->router->generate($page, $args)
        ]);

        $builder = $this->formbuilder->createNamedBuilder('form_' . $nom_formulaire, FormType::class, $data, $options);

        $builder = $this->ajoute_champs($builder, $entree, $nom, 1, $niveau_max);


        return $builder;
    }

    function transforme_data_suivant_entree($nom, $data, $tab_champs)
    {
        foreach ($tab_champs as $champs => $valeur) {

            if (is_array($valeur) && isset($valeur['type_champs'])) {
                if ($valeur['type_champs'] === 'multiple_cle_valeur') {
                    if (is_array($data[$champs])) {
                        foreach ($data[$champs] as $k => &$v) {
                            $v = $k . ':' . $v;
                        }
                        $data[$champs] = implode(PHP_EOL, $data[$champs]);
                    }
                } elseif (in_array($valeur['type_champs'], ['bouton_selection_courante', 'liste_selection_defaut'])) {
                    if (isset($data[$champs]) && is_array($data[$champs])) {
                        $data[$champs] = json_encode($data[$champs]);
                    }
                } elseif ($valeur['type_champs'] === 'multiple') {
                    $data[$champs] = implode(PHP_EOL, $data[$champs]);
                } elseif ($valeur['type_champs'] === 'colonne_affichage') {
                    $nom_objet = explode('.', (string)$nom);
                    if (isset($nom_objet[1]) && $nom_objet[1] === 'show') {
                        $obj = substr($nom_objet[2], 10);
                    } else {
                        $obj = $nom_objet[0];
                    }
                    $tab_colonnes = $this->getTableColonne($obj);
                    if (isset($tab_colonnes['action'])) {
                        unset($tab_colonnes['action']);
                    }
                    $tab_colonnes = array_keys($tab_colonnes);

                    $data_c = [];
                    if (isset($data[$champs])) {
                        $data_c = explode(':', (string)$data[$champs]);
                    }
                    if (is_array($data_c)) {
                        $data[$champs] = array_diff($tab_colonnes, $data_c);
                    } else {
                        $data[$champs] = $tab_colonnes;
                    }

                }
            } elseif (is_array($valeur)) {
                if (isset($data[$champs])) {
                    $data[$champs] = $this->transforme_data_suivant_entree($nom, $data[$champs], $tab_champs[$champs]);
                }
            }

            /*  if (isset($data[$champs]) && empty($data[$champs])) {
                  if (!(isset($valeur['type_champs']) && ($valeur['type_champs'] === 'oui_non' or $valeur['type_champs'] === 'ajout_prestation_type')))
                      $data[$champs] = '0';
              }*/


        }
        return $data;
    }

    function getTableColonne($objet)
    {
        $nom_class = 'App\\Component\\Table\\' . camelize($objet) . 'Table';
        $table = new $nom_class($this->requete, $this->em, $this->sac, $this->suc);
        $tab_colonnes = $table->getColonnes();
        foreach ($tab_colonnes as $k => &$col) {
            $col = $col['title'] ?? $k;
        }
        if (isset($tab_colonnes['action'])) {
            unset($tab_colonnes['action']);
        }
        return $tab_colonnes;

    }

    function ajoute_champs($builder, $tab_champs, $nom_complet = 'xx', $niveau = 1, $niveau_max = 100)
    {

        foreach ($tab_champs as $champs => $valeur) {
            if (is_array($valeur) && !isset($valeur['type_champs'])) {


                if ($niveau < $niveau_max) {

                    $subform = $this->formbuilder->createNamedBuilder(str_replace('.', '_point_', $champs), FormType::class);
                    $subform = $this->ajoute_champs($subform, $valeur, $nom_complet, $niveau + 1, $niveau_max);
                    $builder->add($subform, $champs, ['label' => ' ']);
                }
            } else {
                $class = '';
                $args = [];

                if (is_array($valeur)) {

                    $attr = [];
                    if (isset($valeur['attr'])) {
                        $attr = $valeur['attr'];
                    }
                    if (isset($valeur['class'])) {
                        $class = $valeur['class'];
                    }
                    $opts = [
                        'attr' => $attr,
                        'label' => $champs
                    ];
                    $opts['attr']['class'] = $class;
                    $type_champs = $valeur['type_champs'] ?? 'text';
                    $args = $valeur;
                    $valeur = $valeur['valeur'] ?? '';
                } else {
                    if (is_bool($valeur)) {
                        $type_champs = 'oui_non';
                    } elseif (is_a($valeur, '\DateTime')) {
                        $type_champs = 'date';
                    } elseif (is_int($valeur)) {
                        $type_champs = 'int';
                    } else {
                        $type_champs = 'text';
                    }
                }

                if (is_array($valeur)) {
                    $chaine = "";
                    foreach ($valeur as $kv => $v) {
                        $chaine .= $kv . ':' . $v . PHP_EOL;
                    }
                    $valeur = $chaine;
                }
                $tab_nom = explode('.', (string)$nom_complet);

                [$class_champ, $opts] = $this->type_champs($type_champs, $args, $nom_complet);

                $opts['label'] = $champs;
                if ($class_champ) {

                    $builder->add(str_replace('.', '_point_', $champs), $class_champ, $opts);
                }

                // todo a faire fonctionner avec les lignes 96 97 98 au lieu des lignes 1189 et 200
                // if ($contraints){
                // $builder[$champs]['contraints']=$contraints;
                // }
            }
        }
        return $builder;
    }

    function type_champs($type_champs, $args, $nom_complet)
    {
        $class_champ = TextType::class;
        [$type_champs, $args, $opts] = $this->type_champs_particulier($type_champs, $args, $nom_complet);
        switch ($type_champs) {
            case 'oui_non':
                $class_champ = ChoiceType::class;
                $opts['choices'] = [
                    'oui' => true,
                    'non' => false
                ];
                $opts['label_attr'] = [
                    'class' => 'radio-inline'
                ];
                $opts['expanded'] = true;
                $opts['attr']['inline'] = true;
                break;

            case 'textarea': // observations text area
            case 'multiple': // valeurs
            case 'multiple_cle_valeur': // combo avec couple valeurs M->Monsieur Mme ->Madame
                $class_champ = TextareaType::class;
                $opts['constraints'] = new Assert\NotBlank();
                $opts['attr']['rows'] = $args['nb_ligne'] ?? 5;
                break;

            case 'case_a_cocher':
                $class_champ = ChoiceType::class;
                $opts['choices'] = $args['choices'];
                $opts['multiple'] = true;
                $opts['required'] = false;
                $opts['expanded'] = ($type_champs == 'case_a_cocher');
                break;

            case 'select':
            case 'radio':
            case 'select_multiple':
            case 'radio_multiple':
                if (isset($args['choices']) && count($args['choices']) < 10) {
                    $opts['expanded'] = ($type_champs == 'radio');
                    $opts['label_attr'] = [
                        'class' => ($type_champs == 'radio') ? 'radio-inline' : ''
                    ];
                }
                $opts['multiple'] = false;
                if (in_array($type_champs, [
                    'radio_multiple',
                    'select_multiple'
                ])) {
                    $opts['multiple'] = true;
                }
                $class_champ = ChoiceType::class;
                $opts['choices'] = $args['choices'];
                break;

            case 'fichier':
            case 'fichier_asset':
                $class_champ = FileType::class;
                $opts['attr']['class'] = 'jq-ufs';
                break;

            case 'date':
                $class_champ = DateType::class;
                break;

            case 'couleur':
                $class_champ = ColorType::class;
                break;

            case 'bouton_selection_courante':
                $class_champ = TextareaType::class;
                $opts['attr']['rows'] = $args['nb_ligne'] ?? 5;
                break;

            case 'int':
                $class_champ = IntegerType::class;
                break;

            case 'SubmitType':
                $class_champ = SubmitType::class;
                $opts['attr']['class'] .= ' btn-primary';
                break;

            case '':
                $class_champ = null;
                $opts = null;
                break;
        }
        return [
            $class_champ,
            $opts
        ];
    }

    function type_champs_particulier($type_champs, $args, $nom_complet)
    {
        $opts = [];
        $obj = '';
        foreach (['tri_', 'selection_'] as $prefixe) {
            if (str_starts_with((string)$type_champs, $prefixe)) {
                $obj = substr((string)$type_champs, strlen($prefixe));
                $type_champs = substr((string)$type_champs, 0, strlen($prefixe));
            }
        }

        switch ($type_champs) {
            case 'champ_date_selection':
                $type_champs = 'radio';
                $args['choices'] = [
                    'date_enregistrement' => 'date_enregistrement',
                    'date_debut' => 'date_debut',
                    'date_fin' => 'date_fin'
                ];
                break;
            case 'tableur_export_format':
                $type_champs = 'radio';
                $args['choices'] = [
                    'Text (csv)' => 'csv',
                    'OpenDocument (ods)' => 'ods',
                    'Excel (xls)' => 'xls',
                    'Office Open XML (xlsx)' => 'xlsx'
                ];
                break;

            case 'selection_':
                $type_champs = 'select';
                $args['choices'] = [
                    'courante' => 'courante'
                ];
                $tab_select = $this->suc->pref('selection.' . $obj);
                if (is_array($tab_select)) {
                    foreach ($tab_select as $i => $s) {
                        $args['choices'][$s['nom']] = $i;
                    }
                }
                break;

            case 'tri_':
                $opts = [
                    'attr' => [
                        'class' => 'trier_par',
                        'data-options' => json_encode($this->getTableTri($obj))
                    ]
                ];
                break;


            case 'interactif':
                $type_champs = 'radio';
                $args['choices'] = array_flip($this->getListeValeur('interactif'));
                break;
            case 'modifie':
                $type_champs = 'radio';
                $args['choices'] = array_flip($this->getListeValeur('modification'));
                break;

            case 'colonne_affichage':
                $nom_objet = explode('.', (string)$nom_complet);
                if (isset($nom_objet[1]) && $nom_objet[1] === 'show') {
                    $obj = substr($nom_objet[2], 10);

                } else {
                    $obj = $nom_objet[0];
                }
                $type_champs = 'case_a_cocher';
                $opts = [
                    'attr' => [
                        'class' => 'colonne_affichage',
                    ]
                ];
                $tab_colonnes = $this->getTableColonne($obj);


                $args['choices'] = array_flip($tab_colonnes);
                break;


            case 'mode_saisie':
                $type_champs = 'radio';
                $args['choices'] = [
                    'complete' => 0,
                    'rapide' => 1
                ];
                break;

            case 'majuscule':
                $type_champs = 'radio';
                $args['choices'] = [
                    'Ne rien changer' => 0,
                    'Tout en majuscule' => 1,
                    'Majuscules' => 2
                ];
                break;


            case 'classement_etiquette':
                $type_champs = 'radio';
                $args['choices'] = [
                    'Nom' => 0,
                    'Code postal+ville+nom' => 1,
                    'Ville+nom' => 2
                ];
                break;

            case 'mode_saisie_individu':
                $type_champs = 'radio';
                $args['choices'] = [
                    'nouvel' => 'nouvel',
                    'existant' => 'existant'
                ];
                break;

            case 'type_affichage':
                $type_champs = 'radio';
                $args['choices'] = ['En liste' => 1, 'En grille' => 2];
                break;


            case 'liste_selection_defaut':
                $type_champs = 'textarea';
                break;

            case 'timeline_decoupage':
                $type_champs = 'radio';
                $args['choices'] = [
                    'aucun' => 'aucun',
                    'par jour' => 'jour',
                    'par heure' => 'heure'
                ];
                break;

            case 'timeline_operateur':
                $type_champs = 'radio';
                $args['choices'] = [
                    'toutes les operations' => 'tout',
                    'mes opérations' => 'moi'
                ];
                break;

            case 'timeline_objet':
                $type_champs = 'case_a_cocher';
                $args['choices'] = [
                    'membre' => 'membre',
                    'individu' => 'individu',
                    'prestation' => 'prestation',
                    'les autres' => 'autre'
                ];
                break;


            case 'motgroupe':
                $type_champs = 'case_a_cocher';
                $tab_choice = table_simplifier(table_filtrer_valeur($this->sac->tab('motgroupe'), 'systeme', false));
                if (!empty($tab_choice))
                    $args['choices'] = array_flip($tab_choice);
                else {
                    $type_champs = '';
                }
                break;

        }

        return [$type_champs, $args, $opts];

    }

    function getTableTri($objet)
    {
        $nom_class = 'App\\Component\\Table\\' . camelize($objet) . 'Table';
        $table = new $nom_class($this->requete, $this->em, $this->sac, $this->suc);
        $tab_colonnes = $table->getColonneTriable();
        foreach ($tab_colonnes as $k => &$col) {
            $col = $col['name'] ?? $k;
        }
        return $tab_colonnes;
    }

    function getListeValeur($var, $id = null)
    {
        return getValeur($this->getTabVariables($var), $id);
    }

    // forme mini $builder=builder_ajoute_champs($builder, $tab_champs);
    // tab_champs exemple dans import_form.php
    // nom complet cas particulier de carte_adh1 dans imprimante.etiquette.membre
    // niveau pour arborescence des champs et décalage dans l'affichage

    function getTabVariables($name)
    {
        $tab_vars = [
            'interactif' => [
                //        'modifie' => 'modifie',
                //        'nouveau' => 'nouveau',
                //        'modifie & nouveau' => 'modifie & nouveau',
                //        'aucun' => 'aucun',
                '1' => 'oui',
                '0' => 'non',
            ],
            'modification' => [
                '1' => 'oui',
                '0' => 'non'
            ],
            'apprentissage' => [
                'Mode apprentissage(relevé de banque)' => true,
                'Affectation dans le compte temporaire' => false
            ]

        ];
        return $tab_vars[$name] ?? [];
    }

    function transforme_en_formulaire($nom, $entree, $data, $fonction_sortie, $niveau_max = 1, $args = [], $nom_formulaire = '', $options = [])
    {

        $builder = $this->pretransforme_en_formulaire($nom, $entree, $data, $niveau_max, $args, $nom_formulaire, $options);
        $builder->add('bt_enregistrer', SubmitType::class, ['label' => 'Enregistrer', 'attr' => ['class' => 'btn-primary']]);
        $form = $builder->getForm();

        $form->handleRequest($this->requete->getRequest());
        if ($form->isSubmitted()) {
            $data = $form->getData();
            if ($form->isValid()) {

                $data = $this->conditionne_data_suivant_entree($nom, $data, $entree);
                $nom_fonction = $fonction_sortie . '_enregistrement_result_form';
                $this->$nom_fonction($data, $nom);
            }
        }
        return $form;
    }

    function conditionne_data_suivant_entree($nom, $data, $tab_champs)
    {

        foreach ($tab_champs as $champs => $valeur) {

            if (is_array($valeur) && isset($valeur['type_champs'])) {
                if ($valeur['type_champs'] === 'multiple_cle_valeur') {
                    if (is_string($data[$champs])) {
                        $tab = explode(PHP_EOL, $data[$champs]);
                        $temp_tab = [];
                        foreach ($tab as $t) {
                            [$k, $v] = explode(':', $t);
                            $temp_tab[$k] = $v;
                        }
                        $data[$champs] = $temp_tab;
                    }
                } elseif ($valeur['type_champs'] === 'bouton_selection_courante') {
                    $data[$champs] = json_decode((string)$data[$champs]);
                } elseif ($valeur['type_champs'] === 'colonne_affichage') {

                    $nom_objet = explode('.', (string)$nom);
                    if (isset($nom_objet[1]) && $nom_objet[1] === 'show') {
                        $obj = substr($nom_objet[2], 10);
                    } else {
                        $obj = $nom_objet[0];
                    }
                    $tab_colonnes = $this->getTableColonne($obj);
                    $tab_colonnes = array_keys($tab_colonnes);
                    $data[$champs] = array_diff($tab_colonnes, $data[$champs]);
                    $data[$champs] = implode(':', $data[$champs]);
                } elseif ($valeur['type_champs'] === 'fichier_asset') {
                    $ged = new Ged($this->em, $this->sac);
                    $uploader = new Uploader($ged, $this->sac, $this->requete);
                    $tab_file = $uploader->liste_fichier_avec_path('config');
                    if (!empty($tab_file[0]) && file_exists($tab_file[0])) {
                        $extension = recup_extention_image($tab_file[0]);
                        $path_parts = pathinfo((string)$valeur['nom_destination']);
                        $destination = $path_parts['filename'] . '.' . $extension;
                        rename($tab_file[0], $this->sac->get("dir.root") . "public/assets/perso/" . $destination);
                        $data[$champs] = $destination;
                    } else {
                        $data[$champs] = [];
                    }

                }

            } elseif (is_array($valeur)) {
                $data[$champs] = $this->conditionne_data_suivant_entree($nom, $data[$champs], $tab_champs[$champs]);
            }
        }
        return $data;
    }

    function preference_default_enregistrement_result_form($data, $nom)
    {
        if ($this->ecrire_preference($nom, $data, false, false)) {
            $this->sac->clear();
            $this->sac->initSac();
        }
    }

    /**
     * Mise a jour ou création(absence) de l'enregistrement opérateur et/ou entite la création se refere à l'enregistrement maitre en cas d'absense
     *
     * @param $nom string
     *            sous la forme niveau0[.niveau1[.niveau2]]
     * @param $valeur mixed
     * @param $entite boolean
     * @param $operateur boolean
     *            valeur du dernier niveauX ou tableau complet du dernier niveauX
     * @return bool vrai réussie faux échec todo reste à documenter les erreurs (methode et texte)
     *
     *         Exemple : impimante|etisuette et array('nb_colonne' => 3,'nb_ligne' => 9) change 2 valeurs
     *         ou impimante|etisuette|nb_colonne et 3 Change 1 valeur
     *         ou impimante|etisuette et array('nb_colonne' => 3,'depart' => array('numero'=>4,'autre'=>'test')) change 3 valeurs dont une sous valeur composée de uniquement 2 valeurs si il y en avait plus elle serait perdu
     *         todo voir les valeurs ajoutées dans le maitre aprés la création de l'enregistrement spécifique
     */
    function ecrire_preference($nom, $value, $entite = false, $operateur = true): bool
    {
        if ($nom) {
            $id_utilisateur = $operateur ? $this->suc->get('operateur.id') : null;
            $id_entite = $entite ? $this->suc->get('en_cours.id_entite') : null;
            return $this->suc->ecrire_pref($nom, $value, $id_utilisateur, $id_entite);
        }
        return false;
    }

    function preference_enregistrement_result_form($data, $nom)
    {

        if ($this->ecrire_preference($nom, $data)) {
            $this->suc->initPreference();
        }
    }

    function config_enregistrement_result_form($data, $nom)
    {
        $tab_clef = explode('.', (string)$nom);

        $where = [
            'nom' => $tab_clef[0]
        ];
        $entite = null;
        $config = $this->em->getRepository(Config::class)->findOneBy($where);
        if (!$config) {
            $config = new Config();
            $config->setNom($tab_clef[0]);
            if ($entite) {
                $config->setEntite($this->suc->get('en_cours.id_entite'));
            }
        }
        $config_valeur = $config->getValeur();
        array_shift($tab_clef);

        if (empty($tab_clef)) {
            $config->setValeur($data);
        } else {
            $noeud = implode('.', $tab_clef);
            $config->setValeur(dessinerUneBranche($config_valeur, $noeud, $data));
        }
        $this->em->persist($config);
        $this->em->flush();
        $this->sac->initSac(true);
    }

}


