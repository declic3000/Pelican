<?php

namespace Declic3000\Pelican\Service;


use Doctrine\DBAL\Connection;

class ChargeurDb
{

    protected $db;

    public function __construct(Connection $db)
    {
        $this->db = $db;
    }

    function charger_table($table, $options = [])
    {
        $tab_champs = $options['colonnes'] ?? ['*'];
        $champs = implode(',', $tab_champs);
        $where = (isset($options['where']) && !empty($options['where'])) ? ' WHERE ' . $options['where'] : '';
        $orderby = $options['orderby'] ?? '';

        $sql = 'SELECT DISTINCT ' . $champs . ' FROM ' . $table . $where . $orderby;

        $res = $this->db->fetchAllAssociative($sql);
        $choix = [];
        if (isset($options['cle'])) {
            $cle = $options['cle'];
            if (in_array($cle, $tab_champs) && in_array('nom', $tab_champs) && !isset($options['colonnes'])) {
                foreach ($res as $r) {
                    $choix[$r[$cle] . ''] = $r['nom'];
                }
            } else {
                foreach ($res as $r) {
                    $id = $r[$cle];
                    $choix[$id . ''] = $r;
                }
            }
            return $choix;
        }

        return $res;

    }

    function charger_where($table, $champs, $where)
    {
        $sql = 'SELECT ' . $champs . ' FROM ' . $table . ' WHERE ' . $where;
        return $this->db->fetchAllAssociative($sql);
    }


}
