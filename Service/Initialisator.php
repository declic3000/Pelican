<?php


namespace Declic3000\Pelican\Service;


use App\Init\VariablesApp;
use App\Init\VariablesConfig;
use App\Init\VariablesPreference;
use DateTime;
use Declic3000\Pelican\Event\ConfigModifEvent;
use Declic3000\Pelican\Event\PreferenceModifEvent;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;


class Initialisator
{

    protected $chargeur;
    protected $em;
    protected $db;
    protected $config_version;
    protected $preference_version;
    protected $dispatcher;
    protected $sac;


    function __construct(Sac $sac, EntityManagerInterface $em, EventDispatcherInterface $dispatcher)
    {

        $this->em = $em;
        $this->db = $em->getConnection();
        $this->dispatcher = $dispatcher;
        $this->chargeur = new Chargeur($this->em);
        $this->sac = $sac;
        $this->config_version = $sac->get('app.config_version');
        $this->preference_version = $sac->get('app.preference_version');

    }

    function config_maj()
    {

        $options = $this->sac->get('app');
        $config_initial = $this->getConfigDefaut();

        $where_cplt = '';
        if ($options['utilise_entite']) {
            $where_cplt = ' AND id_entite is null';
        }
        $opt_table_config = $options['table_sys']['config'];
        // Recherche du numero de version de config stocké dans la base de données
        $select = 'SELECT ' . $opt_table_config['cle'] . ' as id,variables 
         FROM ' . $opt_table_config['table_sql'] . '
         WHERE  ' . $opt_table_config['col_name'] . '=' . $this->db->quote('config_version') . $where_cplt;
        $config = $this->db->fetchAssociative($select);
        $num_version_en_cours = '0.0.0';
        $datetime = new DateTime();
        //si l'enregistement du numero de version de la config n'exite pas
        if (empty($config)) {
            $tab_data = [
                $opt_table_config['col_name'] => 'config_version',
                'variables' => json_encode([$num_version_en_cours]),
                'created_at' => $datetime->format('Y-m-d'),
                'updated_at' => $datetime->format('Y-m-d')
            ];
            $this->db->insert($opt_table_config['table_sql'], $tab_data);
            $id_config_version = $this->db->lastInsertId();
        } else {
            $id_config_version = $config['id'];
            $num_version_en_cours = json_decode((string)$config['variables']);
        }

        foreach ($config_initial as $nom => $v) {

            if (isset($v['variables'])) {
                // Mise à jour des configurations générales
                $variables = $this->tableau_epurer($v['variables']);
                $select = 'SELECT ' . $opt_table_config['cle'] . ' as id,variables  FROM ' . $opt_table_config['table_sql'] . ' WHERE ' . $opt_table_config['col_name'] . '=' . $this->db->quote($nom) . $where_cplt;
                $config = $this->db->fetchAssociative($select);
                $tab_data = [
                    $opt_table_config['col_name'] => $nom,
                    'created_at' => $datetime->format('Y-m-d'),
                    'updated_at' => $datetime->format('Y-m-d')
                ];
                if (!empty($opt_table_config['col_observation'])) {
                    $tab_data[$opt_table_config['col_observation']] = 'Configuration par défaut';
                }


                if (empty($config)) { // création
                    $tab_data['variables'] = json_encode($variables);
                    $this->db->insert($opt_table_config['table_sql'], $tab_data);
                } else { // modification

                    $config_variables = json_decode((string)$config['variables'], true);
                    if (is_array($config_variables) && is_array($variables)) {
                        $tab_data['variables'] = $this->table_merge_conf($variables, $config_variables, $v['variables']);
                    } else {
                        $tab_data['variables'] = $variables;
                    }
                    $tab_data['variables'] = json_encode($tab_data['variables']);
                    $this->db->update($opt_table_config['table_sql'], $tab_data, [
                        $opt_table_config['cle'] => $config['id']
                    ]);
                }
                /*
                 * if (isset($v['parametrage_par_entite']) && $v['parametrage_par_entite']) {
                 * $tab_config_ent = ConfigQuery::create()->filterByNom($nom)
                 * ->filterByIdEntite(null, Criteria::ISNOTNULL)
                 * ->find();
                 * foreach ($tab_config_ent as $config_ent) {
                 * $config_variables = $config_ent->getVariables();
                 * if (is_array($config_variables) && is_array($variables)) {
                 * $config_ent->setVariables(table_merge($variables, $config_variables));
                 * } else {
                 * $config_ent->setVariables($variables);
                 * }
                 * $config->save();
                 * }
                 * }
                 */
            }

            $tab_data = [
                'variables' => json_encode([
                    $this->config_version
                ])
            ];
            // modification
            $this->db->update($opt_table_config['table_sql'], $tab_data, [
                $opt_table_config['cle'] => $id_config_version
            ]);
        }
    }

    function getConfigDefaut()
    {
        $config_initial = (new VariablesConfig())->getVariables();
        $event = new ConfigModifEvent($config_initial);
        $this->dispatcher->dispatch($event, 'config.modif');
        return $event->getConfig();
    }

    function tableau_epurer($tab)
    {
        $tab_result = [];
        if (is_array($tab)) {
            if (isset($tab['type_champs'])) {
                return $tab['valeur'] ?? null;
            } else {
                foreach ($tab as $k => $t) {
                    if (is_array($t)) {
                        $tab_result[$k] = $this->tableau_epurer($t);
                    } else {
                        $tab_result[$k] = $t;
                    }
                }
            }
        } else
            return $tab;
        return $tab_result;
    }

    function table_merge_conf($arr1, $arr2, $arr_descr)
    {
        if (is_array($arr1) && is_array($arr2)) {
            if (!empty($arr1)) {


                if (isset($arr_descr['type_champs']) && $arr_descr['type_champs'] === 'multiple_cle_valeur') {
                    $arr1 = $arr2;
                } else {
                    foreach ($arr1 as $key => $value) {
                        if (isset($arr2[$key])) {
                            if (is_array($arr2[$key]) && is_array($value)) {
                                $arr1[$key] = $this->table_merge_conf($arr1[$key], $arr2[$key], $arr_descr[$key]);
                            } elseif (!is_array($arr2[$key]) && !is_array($value)) {
                                $arr1[$key] = $arr2[$key];
                            } else {
                                if (is_a($value, DateTime::class) && $arr2[$key]['date']) {
                                    $arr1[$key] = $arr2[$key];
                                }
                            }


                        } elseif (!is_array($value) && empty($arr2)) {
                            return [];
                        }
                    }
                }
            } else {
                $arr1 = $arr2;
            }
        } elseif (!is_array($arr1) && !is_array($arr2)) {
            $arr1 = $arr2;
        } elseif (!is_array($arr1) && is_array($arr2)) {
            $arr1 = $arr2;
        }
        return $arr1;
    }

    function preference_maj()
    {

        $preference_initial = $this->getPreferenceDefaut();
        $preference_initial = $this->tableau_epurer($preference_initial);


        $options = $this->sac->get('app');
        $where_cplt = '';
        if ($options['utilise_entite']) {
            $where_cplt = ' AND id_entite is null';
        }
        $opt_table_config = $options['table_sys']['config'];
        $opt_table_pref = $options['table_sys']['preference'];

        // Recherche du numero de version des preferences stockées dans la base de données
        $select = 'SELECT ' . $opt_table_config['cle'] . ' as id,variables  FROM ' . $opt_table_config['table_sql'] . ' 
            WHERE ' . $opt_table_config['col_name'] . '=' . $this->db->quote('preference_version') . $where_cplt;
        $preference = $this->db->fetchAssociative($select);
        $num_version_en_cours = '0.0.0';


        //Si l'enregistrement du numéro de version n'existe pas
        if (empty($preference)) {
            $datetime = new DateTime();
            $tab_data = [
                $opt_table_config['col_name'] . '' => 'preference_version',
                'variables' => json_encode([$num_version_en_cours]),
                'created_at' => $datetime->format('Y-m-d'),
                'updated_at' => $datetime->format('Y-m-d')
            ];
            $this->db->insert($opt_table_config['table_sql'], $tab_data);
            $id_preference_version = $this->db->lastInsertId();
        } else {
            $id_preference_version = $preference['id'];
            $num_version_en_cours = json_decode((string)$preference['variables']);
        }
        if ($this->sac->get('app.systeme_pref') === 'table') {

            foreach ($preference_initial as $nom => $v) {

                // Mise à jour des preference utilisateur

                $variables = $this->tableau_epurer($v['variables']);


                $select = 'SELECT ' . $opt_table_pref['cle'] . ' as id,variables  FROM ' . $opt_table_pref['table_sql'] . ' 
                    WHERE ' . $opt_table_pref['col_id_user'] . ' is NULL  and ' . $opt_table_pref['col_name'] . '=' . $this->db->quote($nom) . $where_cplt;
                $pref = $this->db->fetchAssociative($select);
                $tab_data = [
                    $opt_table_pref['col_name'] => $nom
                ];
                if (!empty($opt_table_pref['col_observation'])) {
                    $tab_data[$opt_table_pref['col_observation']] = 'Préférence par défaut';
                }
                if (empty($pref)) { // création

                    $date = new DateTime();
                    $tab_data['created_at'] = $tab_data['updated_at'] = $date->format('Y-m-d h:i:s');
                    $tab_data['variables'] = json_encode($variables);
                    $this->db->insert($opt_table_pref['table_sql'], $tab_data);
                } else { // modification
                    $pref_variables = json_decode((string)$pref['variables']);

                    if (is_array($pref_variables) && is_array($variables)) {
                        $tab_data['variables'] = table_merge($variables, $pref_variables);
                    } else {
                        $tab_data['variables'] = $variables;
                    }

                    $tab_data['variables'] = json_encode($tab_data['variables']);
                    $this->db->update($opt_table_pref['table_sql'], $tab_data, [$opt_table_pref['cle'] => $pref['id']]);
                }


                $variables = $this->tableau_epurer($v['variables']);

                $select = 'SELECT ' . $opt_table_pref['cle'] . ' as id,variables,' . $opt_table_pref['col_id_user'] .
                    '  FROM ' . $opt_table_pref['table_sql'] . ' 
                    WHERE ' . $opt_table_pref['col_id_user'] . ' is not NULL  and ' . $opt_table_pref['col_name'] . '=' . $this->db->quote($nom) . $where_cplt;
                $tab_pref_utilisateur = $this->db->fetchAllAssociative($select);
                $tab_data = [
                    $opt_table_pref['col_name'] => $nom
                ];
                if (!empty($opt_table_pref['col_observation'])) {
                    $tab_data[$opt_table_pref['col_observation']] = 'Préférence par défaut';
                }
                if ($options['utilise_entite']) {
                    $tab_data['id_entite'] = null;
                }
                foreach ($tab_pref_utilisateur as $pref) {
                    if (!empty($pref)) {
                        $pref_variables = json_decode((string)$pref['variables'], true);
                        if (is_array($pref_variables) && is_array($variables)) {
                            $tab_data['variables'] = table_merge($variables, $pref_variables);
                        } else {
                            $tab_data['variables'] = $variables;
                        }
                        $tab_data['variables'] = json_encode($tab_data['variables']);
                        $this->db->update($opt_table_pref['table_sql'], $tab_data, [
                            $opt_table_pref['cle'] => $pref['id']
                        ]);
                    }
                }
            }
        } else {
            foreach ($preference_initial as &$v) {
                $v = $this->tableau_epurer($v['variables']);
            }
            $tab_data = [
                'variables' => json_encode($preference_initial)
            ];

            $this->db->update($opt_table_config['table_sql'], $tab_data, [
                $opt_table_config['col_name'] => 'preference_par_defaut'
            ]);

        }


        // Modification du num de version de la config dans la base
        $tab_data = [
            'variables' => json_encode([
                $this->preference_version
            ])
        ];
        // modification
        $this->db->update($opt_table_config['table_sql'], $tab_data, [
            $opt_table_config['cle'] => $id_preference_version
        ]);

    }

    function getPreferenceDefaut()
    {
        $pref_initial = $this->getVariablesPreference();
        $event = new PreferenceModifEvent($pref_initial);
        $this->dispatcher->dispatch($event, 'preference.modif');
        return $event->getPreference();
    }

    function getVariablesPreference()
    {

        $var_pref = new VariablesPreference();
        $tab_prefs = $var_pref->getVariables();
        $tab_app = (new VariablesApp())->getVariables();
        $tab_objet = array_keys($tab_app['objets']);

        foreach ($tab_objet as $o) {
            if (!isset($tab_prefs[$o])) {
                $tab_prefs[$o] = $var_pref->prepare_modele_objet($o);
            }
        }
        return $tab_prefs;
    }

    function verifier_config()
    {
    }

    function verifier_preference()
    {
    }


}


