<?php

namespace Declic3000\Pelican\Service;


use Doctrine\ORM\EntityManagerInterface;

class Chargeur extends ChargeurDb
{

    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em->getConnection());
        $this->em = $em;
    }

    function charger_objet($objet, $id)
    {
        return $this->em->getRepository('\\App\\Entity\\' . camelize($objet))->find($id);
    }

    function charger_objet_by($objet, $where = [], $order_by = [])
    {
        return $this->em->getRepository('\\App\\Entity\\' . camelize($objet))->findBy($where, $order_by);
    }

    function charger_objet_one_by($objet, $where = [], $order_by = [])
    {
        return $this->em->getRepository('\\App\\Entity\\' . camelize($objet))->findOneBy($where, $order_by);
    }

}

