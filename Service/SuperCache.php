<?php

namespace Declic3000\Pelican\Service;

use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\Cache\Adapter\ApcuAdapter;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\Cache\Adapter\RedisAdapter;
use Symfony\Component\Cache\Exception\CacheException;


class SuperCache
{

    const  SUPER_CACHE = [
        'description',
        'table',
        'pages',
        'droits',
        'app',
        'config',
        'config_tab',
        'preference',
        'preference_meta',
        'dir',
        'systeme'
    ];
    protected AdapterInterface $cache;
    protected array $contexte;

    function __construct(string $cache_system, string $namespace = '')
    {
        $this->initSystemeCache($cache_system, $namespace);
    }

    /**
     * @throws CacheException
     */
    public function initSystemeCache(string $cache_system, string $namespace = '')
    {
        $this->contexte = [];
        $namespace = 'cache_' . sanitize_file_name(normaliser($namespace));
        if ($cache_system === 'apcu') {
            $this->cache = new ApcuAdapter($namespace, 3600 * 24);
        } elseif ($cache_system === 'redis') {
            $client = RedisAdapter::createConnection(
                'redis://localhost'
            );
            $this->cache = new RedisAdapter($client, $namespace, 3600 * 24);
        } else {
            $rep_cache = realpath(__DIR__ . '/../../var/cache_app');
            $this->cache = new FilesystemAdapter($namespace, 3600, $rep_cache);
        }
    }

    /**
     * setContexte
     * @param $var
     * @param $value
     * @return bool new FilesystemAdapter();
     */
    function setContexte($var, $value)
    {

        $path_array = explode('.', (string)$var);
        if (in_array($path_array[0], $this::SUPER_CACHE)) {
            $nom_svar = $path_array[0];
            if (count($path_array) === 1) {
                if (is_array($value)) {
                    $temp_ssc = $this->cache->getItem('soussupercache');
                    $tab_ssc = $temp_ssc->get();
                    foreach ($value as $k => $val) {
                        $this->setContexte($nom_svar . '.' . $k, $val);
                        $tab_ssc[$nom_svar][] = $k;
                    }
                    //completer la liste des soussupercache
                    $temp_ssc->set($tab_ssc);
                    $this->cache->save($temp_ssc);
                }
            } elseif (count($path_array) === 2) {
                $nom_svar = $path_array[0] . '.' . $path_array[1];
                $temp_item = $this->cache->getItem($nom_svar);
                $temp_item->set($value);
                $this->cache->save($temp_item);
                $temp_ssc = $this->cache->getItem('soussupercache');
                $tab_ssc = $temp_ssc->get();
                $tab_ssc[$path_array[0]][] = $path_array[1];
                //completer la liste des soussupercache
                $temp_ssc->set($tab_ssc);
                $this->cache->save($temp_ssc);
            } else {
                $nom_svar = $path_array[0] . '.' . $path_array[1];
                array_shift($path_array);
                array_shift($path_array);
                $temp_a = $this->get($nom_svar);
                $temp = &$temp_a;
                foreach ($path_array as $key) {
                    if (!isset($temp[$key])) {
                        $temp[$key] = [];
                    }
                    $temp = &$temp[$key];
                }
                $temp = $temp_a;

                $temp = $value;
                $temp_item = $this->cache->getItem($nom_svar);
                $temp_item->set($temp);
                $this->cache->save($temp_item);
            }
        } else {
            $this->contexte = dessinerUneBranche($this->contexte, $var, $value);
        }
        return true;
    }

    /**
     * get
     * @param string $nom_var
     * @return array|string|null
     */
    public function get(string $nom_var = '')
    {
        $tab_sc = $this::SUPER_CACHE;
        // Niveau 0
        // On renvoie toutes les variables stockées dans contexte et le supercache
        if ($nom_var === '') {
            $temp = [];
            $temp_ssc = $this->cache->getItem('soussupercache');
            $tab_ssc = $temp_ssc->get();

            foreach ($tab_ssc as $k0 => $tab0) {
                foreach ($tab0 as $v) {
                    $var = $k0 . '.' . $v;
                    if ($this->cache->hasItem($var)) {
                        $temp[$var] = $this->get($var);
                    }
                }
            }
            return array_merge($this->contexte, $temp);
        }

        // Niveau 1
        // On renvoie les series de variables supercache

        $path_array = explode('.', $nom_var);
        $nom_svar = $path_array[0];
        if (in_array($nom_svar, $tab_sc)) {

            if (count($path_array) === 1) {
                $tab_ssc = [];
                if ($this->cache->hasItem('soussupercache')) {
                    $temp_ssc = $this->cache->getItem('soussupercache');
                    $tab_ssc = $temp_ssc->get();
                }
                if (isset($tab_ssc[$nom_svar])) {
                    $temp = [];
                    foreach ($tab_ssc[$nom_svar] as $ssc) {
                        $temp[$ssc] = $this->get($nom_svar . '.' . $ssc);
                    }
                    return $temp;
                }
                return [];
            }

            // Niveau 2 et suivant
            // On renvoie une variables supercache ou un fragment

            else {
                $nom_svar = $path_array[0] . '.' . $path_array[1];

                if ($this->cache->hasItem($nom_svar)) {
                    $temp_item = $this->cache->getItem($nom_svar);
                    $temp = $temp_item->get();
                } else {
                    return null;
                }
                if (count($path_array) == 2) {
                    return $temp;
                }
                array_shift($path_array);
                array_shift($path_array);
                return tableauChemin($temp, $path_array);
            }
        } else {
            if (isset($this->contexte)) {
                $temp = $this->contexte;
            } else {
                return null;
            }
            return tableauChemin($temp, $path_array);
        }
    }

    function clear()
    {
        $this->cache->clear();
        return true;
    }

}
