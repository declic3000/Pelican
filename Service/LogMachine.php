<?php

namespace Declic3000\Pelican\Service;


use App\Entity\Log;
use App\Entity\LogLien;
use DateTime;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\RouterInterface;

class LogMachine
{

    protected bool $actif = false;
    protected $db;
    protected $em;
    protected $sac;
    protected $suc;
    protected $router;
    protected $chargeur;


    function __construct(EntityManagerInterface $em, Sac $sac, Suc $suc, RouterInterface $router)
    {
        if (!$sac->get('app.utilise_log')) {
            return;
        }
        $this->db = $em->getConnection();
        $this->em = $em;
        $this->sac = $sac;
        $this->suc = $suc;
        $this->chargeur = new Chargeur($em);
        $this->router = $router;
        $this->actif = true;
    }

    function estActif(): bool
    {
        return $this->actif;
    }


    function getObjetLog($id = null)
    {

        $tab = [];
        foreach ($this->sac->get('description') as $key => $data) {
            if (isset($data['objet_action'])) {
                $tab[$data['log']] = $data['objet_action'];
            } else {
                $tab[$data['log']] = $key;
            }
        }
        $tab += ($this->sac->get('app.log.code')) ?? [];
        return getValeur($tab, $id);
    }


    function add(
        $sufixe,
        $objet = '',
        $objet_data_ou_id = null,
        $prefixe = '',
        $observation = '',
        $date_param = '',
        $id_utilisateur = -1
    )
    {
        if (!$this->actif){
            return true;
        }
        if (is_bool($sufixe)) {
            $sufixe = ($sufixe) ? 'MOD' : 'NEW';
        }

        if ($objet === '') {
            $objet = $this->sac->get('objet');
            if (!$objet) {
                return false;
            }
        }

        $tab_id_objet = [];
        $id = 0;
        if ($objet_data_ou_id) {
            $temp['objet'] = $objet;
            $objet_data = is_array($objet_data_ou_id) ? $objet_data_ou_id : [$objet_data_ou_id];
            foreach ($objet_data as $obd) {
                if (is_object($obd)) {
                    if (method_exists($obd, 'getPrimaryKey')) {
                        $id = $obd->getPrimaryKey();
                        $temp_class = $obd::class;
                        $ob = decamelize(substr($temp_class, strrpos($temp_class, '\\')));
                        $tab_id_objet[$ob][] = $id;
                    }
                } else {
                    $id = $objet_data_ou_id;
                    $tab_id_objet[$objet][] = $objet_data_ou_id;
                }
            }
        }

        $prefixe = $prefixe ?: $this->sac->descr($objet . '.log');
        $prefixe = strtoupper((string)$prefixe);
        $prefixe = (substr($prefixe . '---', 0, 3));
        $cle_sql = $this->sac->descr($objet . '.cle_sql');
        $code = $prefixe . $sufixe;
        return $this->save($code, [$cle_sql => $id], $tab_id_objet, $observation, $date_param, $id_utilisateur);

    }

    function save(
        $code,
        $variables = [],
        $objets = [],
        $observation = '',
        $date_param = null,
        $id_utilisateur = -1
    )
    {

        if ($id_utilisateur <= 0) {
            $id_utilisateur = $this->suc->get('operateur.id');
        }
        $objet_utilisateur = $this->sac->get('app.table_sys.utilisateur.objet');
        $utilisateur = null;
        if (!empty($objet_utilisateur)) {
            $utilisateur = $this->chargeur->charger_objet($objet_utilisateur, $id_utilisateur);
        }
        $log = new Log();
        $date = new DateTime();
        if ($date_param) {
            $date = $date_param;
        }
        if ($this->sac->get('app.utilise_entite')) {
            $id_entite = ($this->suc->get('en_cours.id_entite')) ?? null;
            if ($id_entite) {
                $entite = $this->chargeur->charger_objet('entite', $id_entite);
                if ($entite) {
                    $log->setEntite($entite);
                }
            }

        }
        if ($utilisateur) {
            $log->setUtilisateur($utilisateur);
        }
        $log->setCode($code);
        $log->setVariables($variables);
        $log->setDateOperation($date);
        $log->setObservation($observation);
        $this->em->persist($log);
        $this->em->flush();
        if ($this->sac->get('app.log.utilise_liens')) {
            $objets = is_array($objets) ? $objets : [$objets];
            foreach ($objets as $obj => $tab_id_objet) {
                if (!is_array($tab_id_objet)) {
                    $tab_id_objet = [$tab_id_objet];
                }
                $tab_id_objet = array_unique($tab_id_objet);
                foreach ($tab_id_objet as $id_objet) {
                    $logliens = new LogLien();
                    $logliens->setLog($log);
                    $logliens->setObjet($obj);
                    $logliens->setIdObjet($id_objet);
                    $this->em->persist($logliens);
                }
            }
        }
        $this->em->flush();
        return $log->getPrimaryKey();
    }

    function erreur($message, $variables = [])
    {


    }

    /**
     * @throws Exception
     */
    function getQuiCree($objet, $id_objet)
    {

        $from = ' FROM ' . $this->sac->descr('log.table_sql');
        if ($this->sac->get('app.log.utilise_liens')) {
            $from .= ' LEFT JOIN ' . $this->sac->descr('loglien.table_sql') . 'll  ON l.id_log= ll.id_log';
        }
        $where = ' WHERE l.code like \'%NEW\' AND ll.objet = ' . $this->db->quote($objet) . ' AND id_objet=' . $id_objet;
        $order = 'ORDER BY l.date_operation ASC ';
        $id_log = $this->db->fetchOne('select id_log' . $from . $where . $order);
        $log = $this->chargeur->charger_objet('log', $id_log);
        if ($id_log) {
            return $log->getIdutilisateur();
        } else {
            return '';
        }
    }


    /**
     * @throws Exception
     */
    function getQuiModifie($objet, $id_objet)
    {
        $from = ' FROM ' . $this->sac->descr('log.table_sql');
        if ($this->sac->get('app.log.utilise_liens')) {
            $from .= ' LEFT JOIN ' . $this->sac->descr('loglien.table_sql') . 'll  ON l.id_log= ll.id_log';
        }
        $where = ' WHERE l.code like \'%EDT\' AND ll.objet = ' . $this->db->quote($objet) . ' AND id_objet=' . $id_objet;
        $order = 'ORDER BY l.date_operation DESC ';
        $id_log = $this->db->fetchOne('select id_log' . $from . $where . $order);
        $log = $this->chargeur->charger_objet('log', $id_log);
        if ($id_log) {
            return $log->getIdutilisateur();
        } else {
            return '';
        }
    }


    /**
     * @throws Exception
     */
    function log_operations($objet, $id_objet): ?array
    {
        $tab_operations = $this->getAllByObjet($objet, $id_objet);
        if (!empty($tab_operations)) {
            return $this->dessineTimeline($tab_operations, 'historique');
        }
        return null;
    }

    /**
     * @throws Exception
     */
    function getAllByObjet($objet = "", $id_objet = 0, $ordre = 'ASC', $id_utilisateur = 0): ?array
    {

        $from = ' FROM ' . $this->sac->descr('log.table_sql') . ' l LEFT JOIN ' . $this->sac->descr('logLien.table_sql') . ' ll  ON l.id_log= ll.id_log';
        $where = ' WHERE  ll.objet = ' . $this->db->quote($objet) . ' AND ll.id_objet=' . $id_objet;
        $order = ' ORDER BY l.date_operation DESC ';
        $tab_id = $this->db->fetchAllAssociative('select l.id_log as id' . $from . $where . $order);
        if (!empty($tab_id)) {
            $tab_id = table_simplifier($tab_id, 'id');
            return $this->chargeur->charger_objet_by('log', ['idLog' => $tab_id], ['dateOperation' => "DESC"]);
        }
        return null;
    }

    function dessineTimeline($tab_operations, $type_affichage = 'timeline'): array
    {

        $tab_op = [];
        $pref = $this->suc->pref('timeline.' . $type_affichage);
        $decoupage = $pref['decoupage'] ?? 'jour';
        $regroupement = $pref['regroupement'] ?? false;
        $descr = $this->sac->descr();
        foreach ($tab_operations as $operation) {
            if ($decoupage === 'jour') {
                $date_groupe = floor(($operation->getDateOperation()->getTimestamp()) / 86400) * 86400;
            } elseif ($decoupage === 'heure') {
                $date_groupe = floor(($operation->getDateOperation()->getTimestamp()) / 3600) * 3600;
            } else {
                $date_groupe = ($operation->getDateOperation()->getTimestamp());
            }
            $code_operation = 'LOG_' . $operation->getCode();
            $variables = $operation->getVariablesEnligne();
            //Regroupement des Logs d'un meme type
            if ($regroupement) {

                if (isset($tab_op[$date_groupe][$code_operation]) or
                    isset($tab_op[$date_groupe]['LOL_' . substr($code_operation, 4)])
                ) {
                    if (isset($tab_op[$date_groupe][$code_operation])) {
                        $tab_op[$date_groupe][$code_operation]['code'] = 'LOL_' . substr($code_operation, 4);
                        $tab_op[$date_groupe]['LOL_' . substr($code_operation, 4)] = $tab_op[$date_groupe][$code_operation];
                        unset($tab_op[$date_groupe][$code_operation]);
                    }
                    $code_operation = 'LOL_' . substr($code_operation, 4);
                }
            }
            $temp = $operation->getUtilisateur();
            $id_utilisateur = $temp ? $temp->getPrimaryKey() : '';
            $operateur = [$id_utilisateur => $this->sac->tab('operateur.' . $id_utilisateur)];//utilisation de tab pour les operateurs supprimés mais encore dans les logs
            if ($regroupement) {
                if (isset($tab_op[$date_groupe][$code_operation]['operateur'])) {
                    $tab_op[$date_groupe][$code_operation]['operateur'] += $operateur;
                } else {
                    $tab_op[$date_groupe][$code_operation] = [
                        'code' => $code_operation,
                        'operateur' => $operateur];
                }
            } else {
                $tab_op[$date_groupe][$operation->getPrimaryKey()] = [
                    'code' => $code_operation,
                    'date' => $operation->getDateOperation(),
                    'operateur' => $operateur];
            }


            // Transformation des id_membre en lien vers les membres
            $lien = '';
            if (is_array($variables)) {

                foreach ($variables as $key => $id) {
                    $ob = substr($key, 3);//enlever id_

                    if (isset($descr[$ob])) {
                        $descr_ob = $descr[$ob];

                        $ob_entity = $descr_ob['objet'];
                        $k = $descr_ob['cle'];
                        $log = !($descr_ob['log'] === false);

                        if ($log) {
                            $ob_tab = '%url_' . $ob_entity . '%';
                            if (in_array($ob, array_keys($descr))) {
                                if ($this->routeExists($ob . '_show')) {
                                    $lien = '<a href="' . $this->router->generate($ob_entity . '_show', [$k => $id]) . '">' . $id . '</a>';

                                }
                            }

                            if (str_ends_with($code_operation, 'DEL')) {
                                $lien = $id;
                            }
                            if ($lien) {
                                if ($regroupement) {
                                    $tab_op[$date_groupe][$code_operation]['variables'][$ob_tab][$id] = $lien;
                                } else {
                                    $tab_op[$date_groupe][$operation->getPrimaryKey()]['variables'][$ob_tab][$id] = $lien;
                                }
                            }
                        }
                    }
                }
            }
        }

        foreach ($tab_op as &$var) {
            foreach ($var as &$var1) {
                if (isset($var1['variables'])) {
                    foreach ($var1['variables'] as &$var2) {
                        $var2 = implode(', ', $var2);
                    }
                }
            }
        }
        return $tab_op;
    }

    function routeExists($name): bool
    {
        // I assume that you have a link to the container in your twig extension class
        $router = $this->router;
        return !((null === $router->getRouteCollection()->get($name)));
    }

    function getTimeline($type = 'defaut'): array
    {
        $pref = $this->suc->pref('timeline.' . $type);
        $id_user = (isset($pref['operateur']) && $pref['operateur'] == 'moi') ? $this->suc->get('operateur.id') : 0;
        $tab_operations = $this->getAll($id_user, 'DESC', $pref['nb_ligne'] ?? 50);
        return $this->dessineTimeline($tab_operations, $type);
    }


    function getAll($id_utilisateur = 0, $ordre = 'ASC', $limite = 30): array
    {
        $req = $this->em->getRepository(Log::class);
        $where = [];
        if ((int)$id_utilisateur > 0) {
            $where = ['utilisateur' => $id_utilisateur];
        }
        return $req->findBy($where, ['dateOperation' => $ordre], $limite);
    }


}
