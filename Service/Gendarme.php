<?php

namespace Declic3000\Pelican\Service;


use Symfony\Component\Security\Core\Authentication\Token\NullToken;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;


class Gendarme implements AuthorizationCheckerInterface
{

    /**
     * @param TokenStorageInterface $tokenStorage
     * @param AccessDecisionManagerInterface $accessDecisionManager An AccessDecisionManager instance
     * @param bool $alwaysAuthenticate
     */
    public function __construct(private readonly TokenStorageInterface $tokenStorage, private readonly AccessDecisionManagerInterface $accessDecisionManager, private readonly Sac $sac, private readonly bool $alwaysAuthenticate = false)
    {
    }

    function autoriser($profils)
    {
        $profils = is_array($profils) ? $profils : [$profils];
        return $this->isGrantedNiveaux($profils);
    }

    /**
     * {@inheritdoc}
     *
     * @throws AuthenticationCredentialsNotFoundException when the token storage has no authentication token
     */
    final function isGrantedNiveaux($tab_attribute)
    {

        foreach ($tab_attribute as $profil) {
            if (!str_starts_with((string)$profil, 'ROLE_')) {
                $profil = 'ROLE_' . $profil;
            }
            if ($this->isGrantedNiveau($profil)) {
                return true;
            }
        }
        return false;
    }

    /**
     * {@inheritdoc}
     *
     * @throws AuthenticationCredentialsNotFoundException when the token storage has no authentication token
     */
    final function isGrantedNiveau($attribute)
    {

        if (!$this->sac->get('app.utilise_droit')) {
            return true;
        }


        if (null === ($token = $this->tokenStorage->getToken())) {
            throw new AuthenticationCredentialsNotFoundException('The token storage contains no authentication token. One possible reason may be that there is no firewall configured for this URL.');
        }

        $tab_roles = $token->getRoleNames();

        $niveau = 0;
        if (!is_array($attribute)) {
            $attribute = [$attribute];
        }

        foreach ($attribute as $attr) {

            $role_simple = strlen((string)$attr) === 7 ? substr((string)$attr, 0, -1) : $attr;

            $n = strlen((string)$attr) === 7 ? (int)substr((string)$attr, -1) : 0;
            foreach ($tab_roles as $role) {
                $matches = [];
                if (preg_match('`^' . $role_simple . '([0-9]{1})$`', $role, $matches) == 1) {
                    $niveau = (int)$matches[1];
                }
            }

            $ok = $this->isGranted($role_simple) && ($n <= $niveau);
            if ($ok) {
                return $ok;
            }
        }
        return false;
    }

    /**
     * {@inheritdoc}
     *
     * @throws AuthenticationCredentialsNotFoundException when the token storage has no authentication token
     */
    public function isGranted($attributes, $subject = null): bool
    {

        if (null === ($token = $this->tokenStorage->getToken())) {
            $token = new NullToken();
        }
        if (!is_array($attributes)) {
            $attributes = [$attributes];
        }
        return $this->accessDecisionManager->decide($token, $attributes, $subject);


    }

}
