<?php

namespace Declic3000\Pelican\Service;

use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Symfony\Contracts\Translation\TranslatorInterface;


class Exporteur
{


    protected $requete;
    protected $sac;
    protected $suc;
    protected $session;
    protected $selecteur;
    protected $db;
    protected $em;
    protected $translator;

    public function __construct(Requete $requete, Suc $suc, Sac $sac, Selecteur $selecteur, EntityManagerInterface $em, TranslatorInterface $translator)
    {
        $this->requete = $requete;
        $this->suc = $suc;
        $this->sac = $sac;
        $request = $requete->getRequest();
        if ($request) {
            $this->session = $request->getSession();
        }
        $this->selecteur = $selecteur;
        $this->db = $em->getConnection();
        $this->em = $em;
        $this->translator = $translator;
    }

    function export_tableur($nom_fichier, $tab_data, $format = 'csv', $save_file = false)
    {

        $nom_fichier .= '.' . $format;
        $rep_cache = $this->sac->get('dir.root') . 'var/print/';
        switch ($format) {
            case 'csv':
                $chaine = '';
                foreach ($tab_data as $ligne) {
                    $chaine .= (str_replace(["\n", "\r", "\""], '', implode("\t", $ligne))) . PHP_EOL;
                }

                if ($save_file) {
                    file_put_contents($rep_cache . $nom_fichier, $chaine);
                } else {
                    header('Content-type: application/vnd.ms-excel');
                    header('Content-disposition: attachment; filename="' . $nom_fichier . '"');
                    echo $chaine;
                }
                break;
            case 'ods':
            case 'xls':
            case 'xlsx':

                $spreadsheet = new Spreadsheet();

                $sheet = $spreadsheet->getActiveSheet();
                $sheet->fromArray($tab_data, null, 'A1');

                $content_type = 'application/vnd.oasis.opendocument.spreadsheet; charset=UTF-8';
                if ($format === 'xls') {
                    $content_type = 'application/vnd.ms-excel; charset=UTF-8';
                } elseif ($format === 'xlsx') {
                    $content_type = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=UTF-8';
                }
                $writer = IOFactory::createWriter($spreadsheet, ucfirst((string)$format));
                if (!$save_file) {
                    header('Content-Type: ' . $content_type);

                    header('Content-Disposition: attachment;filename="' . $nom_fichier . '"');
                    header('Cache-Control: max-age=0');

                    // If you're serving to IE 9, then the following may be needed
                    header('Cache-Control: max-age=1');

                    // If you're serving to IE over SSL, then the following may be needed
                    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
                    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
                    header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
                    header('Pragma: public'); // HTTP/1.0
                    $writer->save('php://output');


                } else {
                    $writer->save($rep_cache . $nom_fichier);
                }
                break;
        }
        if ($save_file) {
            return $rep_cache . $nom_fichier;
        } else {
            exit();
        }
    }


    function export_fin($objet, $tab, $format = 'csv', $message = '')
    {
        $nom_fichier = str_replace(' ', '_', 'liste ' . $this->translator->trans($objet) . ' par ' . $this->suc->get('operateur.nom') . ' le '
            . date('Y-m-d') . '.' . $format);
        $nb_ligne = count($tab);
        if ($format !== 'vcf') {
            if ($message and $nb_ligne < 4) {
                $message .= '<BR />Aucun résultat dans' . $this->translator->trans($objet) . $this->translator->trans(' avec les critères choisis') . '<BR />';
                $this->session->getFlashBag()->add('warning', $message);
                return '';
            }
            $nb_col = count($tab[2]);
            if ($nb_col > 1) {
                for ($i = 0; $i < ($nb_col); $i++) {
                    $lettrefin = chr(65 + fmod($i, 26));
                    $lettredebut = (floor($i / 26)) ? chr(64 + floor($i / 26)) : '';
                }
            }
        }
        switch ($format) {
            case 'csv':
                header('Content-type: application/vnd.ms-excel');
                header('Content-disposition: attachment; filename="' . $nom_fichier . '"');
                foreach ($tab as $ligne) {
                    echo (str_replace(["\n", "\r", "\""], '', implode("\t", $ligne))) . PHP_EOL;
                }
                break;
            case 'ods':
            case 'xls':
            case 'xlsx':

                $spreadsheet = new Spreadsheet();
                $sheet = $spreadsheet->getActiveSheet();
                $sheet->fromArray($tab, null, 'A1');

                $content_type = 'application/vnd.oasis.opendocument.spreadsheet';
                if ($format === 'xls') {
                    $content_type = 'application/vnd.ms-excel';
                }
                if ($format === 'xlsx') {
                    $content_type = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                }
                header('Content-Type: ' . $content_type);
                header('Content-Disposition: attachment;filename="' . $nom_fichier . '"');
                header('Cache-Control: max-age=0');
                // If you're serving to IE 9, then the following may be needed
                header('Cache-Control: max-age=1');
                // If you're serving to IE over SSL, then the following may be needed
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
                header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
                header('Pragma: public'); // HTTP/1.0
                $writer = IOFactory::createWriter($spreadsheet, ucfirst((string)$format));
                $writer->save('php://output');
                break;
            case 'vcf':
                header('Content-Type: text/x-vcard');
                header('Content-Disposition: inline; filename= "' . $nom_fichier . '"');
                echo $tab;
                break;

        }
        exit();

    }


}
