<?php

namespace Declic3000\Pelican\Service;

use App\Init\ChargeurSac;
use Doctrine\DBAL\Connection;
use Symfony\Contracts\Translation\TranslatorInterface;


class Sac extends SuperCache
{


    public $organisation;
    protected $db;
    protected $namespace;
    protected $translator;
    protected $chargeur;
    protected $debug = false;


    function __construct(Connection $db, TranslatorInterface $translator, $application_nom, $organisation, $debug, protected $config_version, protected $preference_version, $cache_system)
    {
        $this->namespace = $application_nom . '_' . ($organisation ?? '') . '_' . $debug;
        $this->organisation = $organisation;
        parent::__construct($cache_system, $this->namespace);
        $this->debug = (bool)$debug;
        $this->db = $db;
        $this->translator = $translator;
        $this->chargeur = new ChargeurDb($db);
        $this->initSac(false);
    }

    public function initSac($force_maj = false)
    {

        if ((!$this->isLoad()) || $force_maj) {
            $root = dirname(__DIR__, 3) . '/';
            $dir = [
                'root' => $root,
                'cache' => $root . 'var/cache/'
            ];
            $chargeur_sac = new ChargeurSac($this->db, new Bigben($this->translator));
            $tab = $chargeur_sac->remplir();
            foreach ($tab as $k => $v) {
                $this->setContexte($k, $v);
            }
            $this->setContexte('dir', $dir);
        }

        return true;
    }

    private function isLoad()
    {
        return !empty($this->get('dir.root'));
    }

    public function changeSystemeCache(string $cache_system, string $namespace = '')
    {
        parent::initSystemeCache($cache_system, $namespace);
        $this->initSac(true);
    }

    public function completeContexteRequete(Requete $requete)
    {

        if ($requete) {
            $page = $requete->getRoute();

            if (is_bool($page)) {
                $page = '';
            }
            $v = ['page' => str_replace('.', '_point_', $page), 'page_groupe' => 'A', 'ajax' => $requete->estAjax(), 'url_courante' => $requete->getUri()];
            // Analyse de l'url
            if ($pos = strrpos((string)$page, '_')) {
                $objet = substr((string)$page, 0, $pos);
                $type_page = substr((string)$page, $pos + 1);

            } else {
                $objet = $page;
                $type_page = 'index';
            }

            if ($objet) {

                $description_objet = $this->descr($objet);
                // on verifie s'il s'agit d'un objet
                if (!$description_objet === false) {
                    // $v['fonction_php'] = $description_objet['php'][$type_page];
                    if (isset($description_objet['alias_valeur'])) {
                        $v['alias_valeur'] = $description_objet['alias_valeur'];
                    }
                    if (isset($description_objet['alias'])) {
                        $v['alias'] = $description_objet['alias'];
                    }

                    if (in_array($type_page, ['show', 'edit', 'mdp', 'dupliquer'])) {

                        $id = $requete->get($description_objet['cle']);
                        if (!$id) {
                            $id = $requete->get('id');
                        }

                        $faire = $type_page;
                        if ($type_page === 'form') {
                            $faire = ($id) ? 'modifier' : 'ajouter';
                        }
                        $v['objet_action'] = $objet . '_' . $faire;
                        $v['id'] = $id;

                        $v['action'] = $faire;
                    }
                    $v['page_groupe'] = $description_objet['groupe'];
                    $v['objet'] = $objet;
                    $v['type_page'] = $type_page;
                } else {
                    // $v['fonction_php'] = str_replace('.', '_point_', $page);
                    $tab_pages = $this->get('page');
                    if (isset($tab_pages[$page]['source'])) {
                        $v['source'] = $tab_pages[$page]['source'];
                    }
                    $v['objet'] = '';
                    $v['type_page'] = 'show';
                }

                $this->contexte = $v;
            }

        }
    }


    /**
     * descr
     *
     * @param string $var
     * @return string|array|null
     */
    function descr(string $var = '')
    {
        return $this->get('description' . (empty($var) ? '' : '.' . $var));
    }

    function charger_table($objet, $options = [])
    {
        $options['cle'] ??= $this->descr($objet . '.cle_sql');
        $table = $options['table'] ?? $this->descr($objet . '.table_sql');
        return $this->chargeur->charger_table($table, $options);
    }


    // Renvoi le resultat d'une selection SQL 

    function charger_objet($objet = null, $id = null)
    {
        $objet = $objet ?: $this->get('objet');
        $id = $id ?: $this->get('id');
        return $this->chargeur->charger_objet($objet, $id);
    }

    // Renvoi une instance d'objet 

    function charger_objet_by($objet = null, $where = [], $order_by = [])
    {
        $objet = $objet ?: $this->get('objet');
        return $this->chargeur->charger_objet_by($objet, $where, $order_by);
    }


    // Renvoi des instances d'objet correspondante 

    /**
     * mot
     *
     * @param $valeur
     * @param string $filtre
     * @param string $retour
     * @return mixed
     */
    function mot($valeur, $filtre = 'nomcourt', $retour = 'id_mot')
    {
        return table_filtrer_valeur_premiere($this->tab('mot'), $filtre, $valeur)[$retour] ?? null;
    }

    /**
     * sac
     *
     * @param string $var
     * @return string|array|null
     */
    public function tab($var = '')
    {
        return $this->get('table.' . $var);
    }

    /**
     * mot
     *
     * @param $valeur
     * @param string $filtre
     * @param string $retour
     * @return mixed
     */
    function motgroupe($valeur, $filtre = 'nomcourt', $retour = 'id_motgroupe')
    {
        return table_filtrer_valeur_premiere($this->tab('motgroupe'), $filtre, $valeur)[$retour] ?? null;
    }


    /**
     * fichier_twig
     *
     * @param string $type_page
     * @param null $objet
     * @return string
     */
    function fichier_twig($type_page = '', $objet = null, $is_form = false): string
    {
        if (!$objet) {
            $objet = $this->get('objet');
        }
        $source = '';
        $page = '';
        if ($objet) {
            $type_page = ($type_page === '') ? $this->get('type_page') : $type_page;
            $page = $this->descr($objet . '.twig.' . $type_page);
            $source = (str_starts_with($page, 'objet')) ? '' : $this->descr($objet . '.source');
        }
        if (empty(trim($page))) {
            if ($is_form) {
                $page = 'inclure/form';
                $source = '';
            } else {
                $page = $this->get('page');
                $source = 'sys/';
            }
        }
        return $source . $page . '.html.twig';
    }


    /**
     * conf
     *
     * @param $var
     * @return array|null|string
     */
    function conf(string $var = '', $defaut = '')
    {
        return $this->lire_config($var, $defaut);
    }


    function lire_config(?string $nom, $defaut = null)
    {
        $val = null;
        if ($nom) {
            $val = $this->get('config.' . $nom);
        } else {
            $val = $this->get('config');
        }
        if ($val === null)
            return $defaut;

        return $val;
    }

    /**
     * @return bool
     */
    public function isDebug(): bool
    {
        return $this->debug;
    }

    /**
     * @param bool $debug
     */
    public function setDebug(bool $debug): void
    {
        $this->debug = $debug;
    }


}