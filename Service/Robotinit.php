<?php

namespace Declic3000\Pelican\Service;

use App\Entity\Tache;
use DateInterval;
use DateTime;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\EntityManagerInterface;

class Robotinit
{

    protected $db;
    protected $sac;
    protected $em;


    function __construct(EntityManagerInterface $em, Sac $sac)
    {
        $this->db = $em->getConnection();
        $this->em = $em;
        $this->sac = $sac;
    }

    /**
     * @throws \Exception
     */
    function traitement_fin_de_tache(): void
    {
        $dir_cache = $this->sac->get('dir.cache');
        $file_lock = $dir_cache . '/tache_lock';
        $tab_tache = $this->em->getRepository(Tache::class)->findBy(['statut' => 4]);
        // Existe t'il des tache effectuée ou en suspension
        if (!empty($tab_tache)) {
            // Supprime les tâches faites
            foreach ($tab_tache as $tache) {
                $this->em->remove($tache);
            }
            $this->em->flush();
        }
        if (file_exists($file_lock)) {
            unlink($file_lock);
        }


        $tab_tache_init = $this->sac->get('app.tache.init') ?? [];


        foreach ($tab_tache_init as $nom => $tachinit) {
            // Ajoute les taches périodiques manquantes
            $nb_tache = $this->db->fetchOne('select count(*) from sys_taches WHERE statut IN(0,1,2) AND fonction=' . $this->db->quote($nom));
            if ($nb_tache == 0) {
                $date_exec = new DateTime();
                $date_exec->add(new DateInterval($tachinit[1]));
                $this->tache_ajouter($nom, $tachinit[0], '', [], [], $date_exec);
            }
        }
        $this->tache_time();
    }

    /**
     * @throws Exception
     */
    function tache_ajouter($fonction, $descriptif, $args = [], $user_var = [], $pref = [], $date_exec = null)
    {

        $nb_tache = $this->db->fetchOne('select count(*) from sys_taches where statut IN(0,1) AND fonction=' . $this->db->quote($fonction));

        if ($nb_tache == 0) {
            $tache = new Tache();
            if ($date_exec === null) {
                $date_exec = new DateTime();
            }
            $tache->fromArray([
                'descriptif' => $descriptif,
                'fonction' => $fonction,
                'args' => $args,
                'statut' => '0',
                'priorité' => '0',
                'dateExecution' => $date_exec
            ]);
            $tache->setSucre([
                'variable' => $user_var,
                'preference' => $pref
            ]);
            $this->em->persist($tache);
            $this->em->flush();
        }
        $this->tache_time();
    }

    function tache_time()
    {
        $tache_time = $this->db->fetchOne('select date_execution from sys_taches WHERE statut IN(0,1,2) ORDER BY date_execution ASC');
        $time = DateTime::createFromFormat('Y-m-d H:i:s', $tache_time)->getTimestamp();
        $this->sac->setContexte('systeme.tache_time', $time);
    }

}
