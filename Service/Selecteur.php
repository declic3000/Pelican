<?php

namespace Declic3000\Pelican\Service;

use Declic3000\Pelican\Query\Query;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;

class Selecteur
{
    protected $objet;
    protected $requete;
    protected $db;
    protected $sac;
    protected $suc;
    protected $querybuilder;
    protected $filtres_statiques = [];
    protected $debug_texte = [];


    public function __construct(Requete $requete, Connection $db, Sac $sac, Suc $suc)
    {
        $this->db = $db;
        $this->requete = $requete;
        $this->sac = $sac;
        $this->suc = $suc;
        if ($this->objet) {
            $this->initQuerybuilder();
        }
    }

    protected function initQuerybuilder()
    {
        $nomclassQuery = '\App\Query\\' . ucfirst((string)$this->objet) . 'Query';
        if (class_exists($nomclassQuery)) {
            $this->querybuilder = new $nomclassQuery($this->objet, $this->requete, $this->db, $this->sac, $this->suc);
        } else {
            $nomclassQuery = Query::class;
            $this->querybuilder = new $nomclassQuery($this->objet, $this->requete, $this->db, $this->sac, $this->suc);
        }
    }

    public function getObjet()
    {
        return $this->objet;
    }

    public function setObjet($objet)
    {
        $this->objet = $objet;
        if ($this->objet && $this->db) {
            $this->initQuerybuilder();
        }
    }

    public function setFiltresStatiques($filtres)
    {
        $this->filtres_statiques = $filtres;
    }

    /**
     *  Renvoi le nombre total de ligne de resultat
     *
     * @param array $params tableau de parametres qui surcharge ceux passés dans l'URL
     * @return int le nombre total de ligne
     */
    public function getNb($params = []): int
    {
        [$req, $nb] = $this->getSelectionObjetNb($params);
        return $nb;
    }

    /**
     *  Contruit la requete sur un objet et renvoi le nombre total de ligne de resultat
     *
     * @param array $params tableau de parametres qui surcharge ceux passés dans l'URL
     * @param null $tab_cols les colonne souhaitées
     * @param null $tri nom d'un objet ou tableau de nom d'objet pour jointure
     *
     * @return array [0=> la requete SQL de selection, 1=> le nombre total de ligne]
     */
    public function getSelectionObjetNb($params = [], $tab_cols = null, $tri = null)
    {
        if (isset($params['where'])) {
            if (isset($params['from'])) {
                $from = $params['from'];
            } else {
                $from = [];
            }
            $where = $params['where'];
        } else {
            [$from, $where] = $this->construireWhere($params);
        }

        return $this->getRequeteObjetNb($from, $where, $tab_cols);
    }

    public function construireWhere($params = [], $liaison = []): array
    {

        [$tab_liaisons, $where] = $this->querybuilder->getWhere($params);
        $tab_liaisons += $liaison;

        if (!empty($this->filtres_statiques)) {
            [$tab_liaisons_statique, $tab_where_statique] = $this->querybuilder->getWhere($this->filtres_statiques);

            $tab_liaisons = array_merge($tab_liaisons, $tab_liaisons_statique);

            $where = array_unique(array_merge($where, $tab_where_statique));
        }


        $from = $this->sql_jointure($this->objet, $tab_liaisons);


        $temp = $this->where_restriction();

        if (!empty($temp)) {
            $where[] = $temp;
        }


        $where = implode(' AND ', $where);


        $temp = $this->selection_ajouter_limitation_id($this->objet, $params);
        if (!empty($temp)) {
            $where .= ' AND ' . $temp;
        }

        if ($num_query = $this->requete->ouArgs('query', $params)) {
            $pref = $this->suc->pref('query.' . $this->objet . '.' . $num_query);
            if (isset($pref['query'])) {
                $where .= (($where == '') ? '' : ' AND ') . ' ' . $this->construireArgsQuery($pref);
            }
        }
        if ($this->requete->ouArgs('inverse_selection', $params)) {
            $where = 'NOT (' . $where . ')';
        }


        return [$from, $where];


    }

    public function sql_jointure($objet, $tab_liaisons, $prefixe = '', $prefixe0 = null, $operateur = ' LEFT OUTER JOIN '): array
    {
        $join = [];
        foreach ($tab_liaisons as $i => $l) {
            $l = (is_array($l)) ? $l : [$i => $l];
            $join += $this->sql_ecrire_join($objet, $l, $prefixe, $prefixe0, $operateur);
        }

        return array_unique($join);
    }

    public function sql_ecrire_join($objet, $chemin_liaison = [], $prefixe_tri = '', $prefixe_tri0 = null, $operateur = ' LEFT OUTER JOIN ')
    {
        $tab = [];
        $nb_liaison = count($chemin_liaison);
        $objet_a = $objet;
        $i = 0;

        if ($prefixe_tri0 === null) {
            $prefixe_tri0 = $prefixe_tri;
        }
        $nom_sql_a = $prefixe_tri0 . $this->sac->descr($objet_a . '.nom_sql');

        foreach ($chemin_liaison as $iliaison => $chemin) {


            if (is_array($chemin)) {
                $nom_liaison = (is_int($iliaison)) ? key($chemin) : $iliaison;
                $chemin_objet = key($chemin);
                $liaison_val = current($chemin);
            } else {
                $nom_liaison = (is_int($iliaison)) ? $chemin : $iliaison;
                $chemin_objet = $chemin;
                $liaison_val = [];
            }
            $nom_liaison_indice = $prefixe_tri . $nom_liaison;


            $nom_class = '\App\Query\\' . $this->sac->descr($objet_a . '.phpname') . 'Query';

            if (property_exists($nom_class, 'liaisons')) {
                $var_l = $nom_class::$liaisons;

                if (isset($var_l[$chemin_objet])) {

                    $liaison = $var_l[$chemin_objet];

                    if (isset($liaison['valeurs'])) {
                        $liaison_val = array_merge($liaison['valeurs'], $liaison_val);
                    }


                    if (isset($liaison['objet'])) {
                        $objet_b = $liaison['objet'];
                        $table_b = $this->sac->descr($objet_b . '.table_sql');
                        $nom_sql_b = $this->sac->descr($objet_b . '.nom_sql');
                        $table_lien = $table_b . '_liens';
                        //$nom_sql_b = ($i == ($nb_liaison - 1) && !empty($nom_sql)) ? $prefixe_tri . $nom_sql : $prefixe_tri . $nom_sql_b;
                        $nom_sql_b = $nom_liaison_indice;
                        $nom_sql_lien = $nom_liaison_indice . '_liens';

                    } else {

                        $objet_b = $liaison['table'];

                        $table_b = $liaison['table'];
                        //$nom_sql_b = $this->sac->descr($objet_b . '.nom_sql')?? $iliaison;
                        //$nom_sql_b = ($i == ($nb_liaison - 1) && !empty($nom_sql)) ? $nom_sql : $objet_b;
                        $nom_sql_b = $nom_liaison_indice;
                        $nom_sql_lien = $nom_liaison_indice . '_l';
                    }

                    $champs_a = $liaison['local'];
                    $champs_b = $liaison['foreign'];
                    $critere = '';
                    $critere_lien = '';

                    if (!empty($liaison_val)) {
                        foreach ($liaison_val as $champs => $valeur) {

                            if (is_array($valeur)) {
                                $temp = ' IN (' . implode(',', $valeur) . ')';
                            } elseif (is_numeric($valeur)) {
                                $temp = '=' . $valeur;
                            } else {
                                $temp = '=' . '\'' . $valeur . '\'';
                            }
                            $critere .= ' AND ' . $nom_sql_b . '.' . $champs . $temp;

                            if ($champs_b == $champs) {
                                $critere_lien = ' AND ' . $nom_sql_lien . '.' . $champs . $temp;
                            }
                        }
                    }

                    if (isset($liaison['table_relation']) && $liaison['table_relation']) {
                        $tab[$nom_sql_lien] = $operateur . $liaison['table_relation'] . ' ' . $nom_sql_lien . '  ON ' . $nom_sql_a . '.' . $champs_a . ' = ' . $nom_sql_lien . '.' . $champs_a . $critere_lien;
                        $tab[$nom_liaison_indice] = $operateur . $table_b . ' ' . $nom_sql_b . '  ON ' . $nom_sql_lien . '.' . $champs_b . ' = ' . $nom_sql_b . '.' . $champs_b . $critere;
                    } elseif (isset($liaison['table_liens']) && $liaison['table_liens']) {
                        $tab[$nom_liaison_indice . '_liens'] = $operateur . $liaison['table_liens'] . ' ' . $nom_sql_lien . '  ON ' . $nom_sql_lien . '.objet = \'' . $objet_a . '\' AND ' . $nom_sql_a . '.' . $champs_a . ' = ' . $nom_sql_lien . '.id_objet' . $critere_lien;
                        $tab[$nom_liaison_indice] = $operateur . $table_b . ' ' . $nom_sql_b . '  ON ' . $nom_sql_lien . '.' . $champs_b . ' = ' . $nom_sql_b . '.' . $champs_b . $critere;
                    } else {
                        $tab[$nom_liaison_indice] = $operateur . $table_b . ' ' . $nom_sql_b . '  ON ' . $nom_sql_a . '.' . $champs_a . ' = ' . $nom_sql_b . '.' . $champs_b . $critere;
                    }
                    $objet_a = $objet_b;
                    $nom_sql_a = $nom_sql_b;
                }
            }
            //$prefixe_tri0 = $prefixe_tri;
            $i++;
        }
        return $tab;
    }

    /**
     * //$direct a true renvoi le tableau de id
     * //  sinon renvoi un complement de where utilisat l'alis de l'objet pour le fichier principal
     *
     *
     * @return null|array|string
     */
    public function where_restriction(): array|string|null
    {
        return $this->suc->get('restriction_where.' . $this->suc->get('en_cours.id_entite') . '.' . $this->sac->descr($this->objet . '.groupe') . '.' . $this->objet);
    }

    public function selection_ajouter_limitation_id($objet, $params)
    {

        $where = '';
        $pr = $this->sac->descr($objet . '.nom_sql');
        $cle = $this->sac->descr($objet . '.cle_sql');
        $restreindre_id = $this->requete->ouArgs('limitation_id', $params);

        if (!empty($restreindre_id)) {
            if (!str_contains((string)$restreindre_id, ',')) {
                $restreindre_id = [$restreindre_id];
            } else {
                $restreindre_id = explode(',', (string)$restreindre_id);
            }
            $where = ' ( ';
            foreach ($restreindre_id as $key => $id) {
                if ($key != 0) {
                    $where .= ' or ';
                }
                if (!str_contains((string)$id, '-')) {
                    $where .= $pr . '.' . $cle . ' =' . $id;
                } else {
                    $ids = explode('-', (string)$id);
                    $where .= '(' . $pr . '.' . $cle . ' BETWEEN ' . $ids[0] . ' AND ' . $ids[1] . ')';
                }

            }
            $where .= ')';
        }
        return $where;
    }

    public function construireArgsQuery($pref): string
    {

        $query = json_decode((string)$pref['query'], true);
        $condition = $query['condition'];
        $where = '';
        $not = (isset($query['not']) && $query['not']) ? ' NOT ' : '';
        $where1 = [];


        foreach ($query['rules'] as $k => $rule) {

            if (isset($rule['id'])) {
                $where1[] = $this->ajouterArgsQuery($rule, $k);
            } else {
                $condition2 = $rule['condition'];
                $not2 = (isset($rule['not']) && $rule['not']) ? ' NOT ' : '';
                $where2 = [];
                foreach ($rule['rules'] as $i => $r) {
                    $where2[] = $this->ajouterArgsQuery($r, $k . $i);
                }
                $where1[] = $not2 . ' (' . implode(' ' . $condition2 . ' ', $where2) . ')';
            }

        }


        $where .= $not . ' (' . implode(' ' . $condition . ' ', $where1) . ')';


        return $where;
    }

    public function ajouterArgsQuery($rule, $k): string
    {
        $nom_sql = $this->sac->descr($this->objet . '.nom_sql');
        $cle_sql = $this->sac->descr($this->objet . '.cle_sql');
        $table_sql = $this->sac->descr($this->objet . '.table_sql');

        $p = [$rule['id'] => $rule['value']];
        $not = (isset($rule['not']) && $rule['not']) ? ' NOT ' : '';
        $p_options = [$rule['id'] => $rule['operator']];
        $nom_liaison = 'query' . chr($k + 65);
        [$from0, $where0] = $this->querybuilder->getWhere($p, $p_options, '');
        $from1[$nom_liaison] = $from0;
        $tab_join = $this->sql_jointure($this->objet, $from0, '', $nom_liaison);
        $join = implode(' ', $tab_join);
        return '(' . $nom_sql . '.' . $cle_sql . '' . $not . ' IN (SELECT ' . $nom_liaison . $nom_sql . '.' . $cle_sql . ' FROM ' . $table_sql . ' ' . $nom_liaison . $nom_sql . ' ' . $join . ' WHERE ' . implode(' AND ', $where0) . '))';

    }

    /**
     * @param $from
     * @param $where
     * @param null|array $tab_cols
     * @param string $preprefixe
     *
     * @return array
     */
    public function getRequeteObjetNb($from, $where, ?array $tab_cols = null, string $preprefixe = '')
    {

        $select = $this->getRequeteObjet($from, $where, $tab_cols, $preprefixe);
        $nom_class = $this->sac->descr($this->objet . '.phpname') . 'Query';
        if (property_exists($nom_class, 'filtrage_entite')) {
            if ($nom_class::$filtrage_entite) {
                $where .= ' AND id_entite=' . $this->suc->get('en_cours.id_entite');
            }
        }
        $where = empty($where) ? '' : ' WHERE ' . $where;
        $select_count = 'SELECT COUNT( distinct ' . $this->sac->descr($this->objet . '.nom_sql') . '.' . $this->sac->descr($this->objet . '.cle_sql') . ') as nb_total FROM ' . $this->sac->descr($this->objet . '.table_sql') . ' ' . $this->objet . ' ' . implode(' ', $from) . $where;
        return [$select, (int)$this->db->fetchOne($select_count)];
    }

    /**
     * @param $left
     * @param $where
     * @param null|string|array $tab_cols
     * @param string $preprefixe
     *
     * @return string
     */
    public function getRequeteObjet($left, $where, $tab_cols = null, $preprefixe = '')
    {

        if (empty($tab_cols)) {
            $pr = $this->sac->descr($this->objet . '.nom_sql');
            $cle = $this->sac->descr($this->objet . '.cle_sql');
            $tab_cols = $pr . '.' . $cle;
        } elseif (is_array($tab_cols)) {
            $tab_cols = implode(',', $tab_cols);
        }

        $where = empty($where) ? '' : ' WHERE ' . $where;
        return 'SELECT DISTINCT ' . $tab_cols . ' FROM ' . $this->sac->descr($this->objet . '.table_sql') . ' ' . $preprefixe . $this->objet . ' ' . implode(' ', $left) . $where;
    }

    public function cmp($a, $b): int
    {
        return strcmp($a->primaryKey, $b->primaryKey);
    }

    /**
     *
     * @param array $args
     * @param string $cle
     *
     * @return array
     */
    public function getTabId($args = [], $cle = ''): array
    {
        if (empty($cle)) {
            $cle = $this->sac->descr($this->objet . '.cle_sql');
        }

        $sql = $this->getSelectionObjet($args, $cle);
        $tab_id = $this->db->fetchAllAssociative($sql);
        return table_simplifier($tab_id, $cle);
    }

    /**
     * Contruit la requete sur un objet
     *
     * @param array $params tableau de parametres qui surcharge ceux passés dans l'URL
     * @param null|array|string $tab_cols les colonne souhaitées
     * @param null|array $liaison nom d'un objet ou tableau de nom d'objet pour jointure
     *
     * @return string la requete SQL de selection
     */
    public function getSelectionObjet(array $params = [], $tab_cols = null, ?array $liaison = []): string
    {
        [$from, $where] = $this->construireWhere($params, $liaison);
        return $this->getRequeteObjet($from, $where, $tab_cols);
    }

    /**
     * @param null $cle_result
     *
     * @return array
     */
    public function rechercher($cle_result = null): array
    {

        $cle = $this->sac->descr($this->objet . '.cle_sql');
        $sous_requete = $this->getSelectionObjet();
        $tab_data = [];
        $nb_max = 10;
        $search = $this->requete->get('search');
        if ($search) {
            $recherche = trim((string)$search['value']);
            if (strlen($recherche) > 1) {
                $nb_max = 100;
            }
        }
        $tab_id = $this->db->fetchAllAssociative($sous_requete . ' LIMIT 0,' . $nb_max);
        $tab_id = table_simplifier($tab_id, $cle);

        foreach ($tab_id as $t) {
            $tab_data[] = [
                'objet' => $this->objet,
                'id' => $t['id'],
                'nom' => $t['nom'],
                'description' => $t['description']
            ];
        }
        return $tab_data;
    }

    /**
     * $direct a true renvoi le tableau de id
     * sinon renvoi un complement de where utilisat l'alis de l'objet pour le fichier principal
     *
     * @param array $args_restrictions
     *
     * @return array|string
     */
    public function calcule_where_restriction(array $args_restrictions)
    {

        $sql = '';

        if (!empty($args_restrictions)) {
            $this->requete->setBloquer(true);
            [$tab_liaisons, $where] = $this->querybuilder->getWhere($args_restrictions, [], 'restriction_');
            $this->requete->setBloquer(false);


            $from = $this->sql_jointure($this->objet, $tab_liaisons, 'restriction_', 'restriction_');

            $where = implode(' AND ', $where);
            $cle_sql = $this->sac->descr($this->objet . '.cle_sql');
            $nom_sql = $this->sac->descr($this->objet . '.nom_sql');
            $sous_requete_restriction = $this->getRequeteObjet($from, $where, 'restriction_' . $nom_sql . '.' . $cle_sql, 'restriction_');
            $sql = $nom_sql . '.' . $cle_sql . ' IN (' . $sous_requete_restriction . ')';
        }
        return $sql;
    }

    /**
     * tri_sql
     * @param $tab_tri
     * @return array
     */
    public function tri_sql($objet, $tab_tri): array
    {
        $prefixe_tri = 'tri_';
        $objet = is_array($objet) ? $objet[0] : $objet;
        $left_join = [];
        $tri_sql = "";

        if (is_array($tab_tri) && !empty($tab_tri)) {
            $tri_sql = ' ORDER BY ';
            $tab = [];
            if (!$objet) {
                $objet = $this->sac->get('objet');
            }

            foreach ($tab_tri as $k => $odr) {
                $ok = false;

                if (isset($this->trier_par)) {

                    $tab_trier_par = $this->trier_par;

                    if (isset($tab_trier_par[$k])) {


                        if (isset($tab_trier_par[$k]['liaisons'])) {

                            $left_join += $this->sql_ecrire_join($objet, $tab_trier_par[$k]['liaisons'], $prefixe_tri, '');


                            $prefixe_tri0 = $prefixe_tri;

                            if (!empty($tab_trier_par[$k]['champs'])) {
                                $tab_c = $tab_trier_par[$k]['champs'];
                                $tab_c = is_array($tab_c) ? $tab_c : [$tab_c];
                                foreach ($tab_c as $champs) {
                                    $tab[] = $prefixe_tri0 . $champs . ' ' . $odr;
                                }
                            }
                        } else {
                            $tab[] = decamelize($k) . ' ' . $odr;
                        }
                        $ok = true;
                    }
                }
                if (!$ok) {
                    $indice = strpos($k, '.');
                    if ($indice !== false) {
                        $nom_classe = $this->getQuerybuilder();
                        $liaisons = $nom_classe::$liaisons;
                        $ob = substr($k, 0, $indice);
                        $left_join += $this->sql_ecrire_join($objet, [$ob], $prefixe_tri, '');
                        if (isset($liaisons[$ob]['objet'])) {
                            $tab[] = $prefixe_tri . $this->sac->descr($liaisons[$ob]['objet'] . '.nom_sql') . '.' . decamelize(substr($k, $indice)) . ' ' . $odr;
                        }
                    } else {
                        $tab[] = $this->sac->descr($objet . '.nom_sql') . '.' . decamelize($k) . ' ' . $odr;
                    }
                }
            }
            if (!empty($tab)) {
                $tri_sql .= implode(',', $tab);
            } else {
                $tri_sql = '';
            }

        }
        if (!empty($left_join)) {
            $left_join = array_unique($left_join);
            //$left_join = implode(' ', $left_join);
        } else {
            // $left_join = '';
        }

        return [$tri_sql, $left_join];
    }

    public function getQuerybuilder()
    {
        return $this->querybuilder;
    }

    public function liste_autocomplete($limit = 10): array
    {

        $descr = $this->sac->descr($this->objet);
        $nom_colonne_cle = $this->nom_colonne_code ?? $descr['cle_sql'];
        $nom = is_array($descr['champs_nom']) ? $descr['champs_nom'] : [$descr['champs_nom']];
        $pr = $descr['objet'];
        $sous_requete = $this->getSelectionObjet([], $nom_colonne_cle . ' as id, CONCAT(' . $pr . '.' . implode(',' . $pr . '.', $nom) . ') as nom');

        $tab = $this->db->fetchAllAssociative($sous_requete . ' LIMIT 0,' . $limit);
        $tab_data = [];
        foreach ($tab as $el) {
            $tab_data[] = [
                'id' => $el['id'],
                'text' => trim((string)$el['nom']),
                //'url'=>''//$app->path('membre',['id_membre'=>$membre->getPrimaryKey()]),
                //'description'=>'',
            ];
        }
        return $tab_data;
    }


    /**
     * @throws Exception
     */
    public function liste_rechercher($limit = 10): array
    {
        $pr = $this->sac->descr($this->objet . '.nom_sql');
        $nom = $this->sac->descr($this->objet . '.champs_nom');
        $nom = is_array($nom) ? $nom : [$nom];
        $sql = $this->getSelectionObjet([], $this->sac->descr($this->objet . '.cle_sql') .
            ' as id, CONCAT(' . $pr . '.' . implode(',' . $pr . '.', $nom) . ') as nom');
        $tab = $this->db->fetchAllAssociative($sql . ' LIMIT 0,' . $limit);
        foreach ($tab as $el) {
            $tab_data[] = [
                'id' => $el['id'],
                'nom' => trim((string)$el['nom']),
                //'url'=>''//$app->path('membre',['id_membre'=>$membre->getPrimaryKey()]),
                //'description'=>'',
            ];
        }
        return $tab;
    }


    /**
     *
     */
    public function ajoute_debug($texte)
    {
        if ($this->sac->isDebug()) {
            $this->debug_texte[] = $texte;
        }
        return true;
    }


    /**
     *
     */
    public function ajouter_au_debug(string $texte): void
    {
        if ($this->sac->isDebug()) {
            $this->debug_texte[] = $texte;
        }
    }

    /**
     *
     */
    public function affiche_debug()
    {
        if ($this->sac->isDebug()) {
            return $this->debug_texte;
        }
        return null;
    }

}
