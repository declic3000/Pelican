<?php

namespace Declic3000\Pelican\Service;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;


class Requete
{

    protected $bloquer = false;
    protected $sauf_args = [];
    protected $requestStack;

    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    public function getRoute()
    {
        return ($this->getRequest())->attributes->get('_route');
    }

    public function get(string $key, $default = null)
    {
        $requete = $this->getRequest();
        if ($requete) {
            return $requete->get($key, $default);
        } else {
            return $default;
        }
    }

    public function getRequest(): ?Request
    {
        return $this->requestStack->getCurrentRequest();
    }

    public function getRequestStack()
    {
        return $this->requestStack;
    }

    public function all(string $key, $default = null)
    {
        $requete = $this->getRequest();
        if ($requete) {
            $value = $requete->request->all($key);
            if ($value) {
                return $value;
            } else {
                return $default;
            }
        } else {
            return $default;
        }
    }


    public function getUri()
    {
        return ($this->getRequest())->getUri();
    }

    public function estAjax()
    {
        return ($this->getRequest())->isXmlHttpRequest();
    }

    /**
     * @return bool
     */
    public function isBloquer(): bool
    {
        return $this->bloquer;
    }

    /**
     * @param bool $bloquer
     */
    public function setBloquer(bool $bloquer, $sauf_args = []): void
    {
        $this->bloquer = $bloquer;
        $this->sauf_args = $sauf_args;
    }

    public function ouArgs($nom, $params = [])
    {
        $requete = ($this->getRequest());
        if (!isset($params[$nom])
            && !$this->argsBloquer($nom)
            && $requete
            && $value = $requete->get($nom)) {
            return $value;
        } elseif (isset($params[$nom])) {
            return $params[$nom];
        }
        return '';
    }

    /**
     * @return bool
     */
    public function argsBloquer($nom_args): bool
    {
        return $this->bloquer && (!in_array($nom_args, $this->sauf_args));
    }

}
