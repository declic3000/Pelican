<?php

namespace Declic3000\Pelican\Service;


use RuntimeException;

class Uploader
{
    protected $session;
    protected $path_upload;
    protected $ged;


    function __construct(Ged $ged, Sac $sac, Requete $requete)
    {

        $this->session = $requete->getRequest()->getSession();
        $dir = $sac->get('dir.root');
        $this->ged = $ged;

        $this->path_upload = $dir . 'var/upload';
        if (!file_exists($this->path_upload)) {
            if (!mkdir($concurrentDirectory = $this->path_upload) && !is_dir($concurrentDirectory)) {
                throw new RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
            }
        }

    }

    function traitement_form_ged($nom_champs, $id_objet, $objet_nom = 'individu', $utilisation = "", $multiple = false)
    {
        $tab_fichier = $this->liste_fichier_avec_path($nom_champs);
        if (!empty($tab_fichier)) {
            $this->ged->enregistre_fichiers($tab_fichier, $id_objet, $objet_nom, $utilisation, $multiple);
            $this->vide_liste($nom_champs);
            return true;
        }
        return false;
    }

    function liste_fichier_avec_path($nom_champs)
    {
        $tab_fichier = $this->liste_fichier($nom_champs);
        if (!empty($tab_fichier)) {
            foreach ($tab_fichier as &$fichier) {
                $fichier = $this->path_upload . '/' . $fichier;
            }
        }
        return $tab_fichier;
    }


    function liste_fichier($nom_champs)
    {
        return $this->session->get('file_upload_tmp_' . $nom_champs);
    }

    function vide_liste($nom_champs)
    {
        $this->session->remove('file_upload_tmp_' . $nom_champs);
    }

    function liste_ajoute_fichier($nom_champs, $tab_fichiers)
    {
        $this->session->set('file_upload_tmp_' . $nom_champs, $tab_fichiers);
    }

}

