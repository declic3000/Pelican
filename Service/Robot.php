<?php

namespace Declic3000\Pelican\Service;

use App\Entity\Tache;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Translation\TranslatorInterface;

class Robot extends Robotinit
{

    protected $facteur;
    protected $log;
    protected $suc;
    protected $router;

    function __construct(EntityManagerInterface $em, Sac $sac, Suc $suc, Facteur $facteur, LogMachine $log, RouterInterface $router)
    {
        parent::__construct($em, $sac);
        $this->facteur = $facteur;
        $this->log = $log;
        $this->suc = $suc;
        $this->router = $router;
    }

    function execute_tache(Tache $tache_ent)
    {

        $sucre = $tache_ent->getSucre();
        $user_val = $sucre['variable'] ?? [];
        $pref = $sucre['preference'] ?? [];
        $this->suc->setModeGlucose(true);
        $this->suc->loadValeur(['variable' => $user_val, 'preference' => $pref]);
        $nom_class = "\\App\\Tache\\" . camelize($tache_ent->getFonction()) . 'Tache';
        if (property_exists($nom_class, 'log')) {
            $tache = new $nom_class($this->sac, $this->suc, $this->em, $this->facteur, $this->log, $this->router);
        } else {
            $tache = new $nom_class($this->sac, $this->em);
        }
        $this->em->persist($tache_ent);
        $this->em->flush();
        $args = $tache_ent->getArgs();
        $tache->tache_init($tache_ent->getAvancement(), $args);
        $fini = $tache->tache_run();
        $avancement = $tache->getAvancement();
        $tache_ent->setAvancement($avancement);
        $tache_ent->setStatut($fini ? 4 : 2);
        $this->em->persist($tache_ent);
        $this->em->flush();
        $this->traitement_fin_de_tache();
    }


}
