<?php

namespace Declic3000\Pelican\Service;

use App\Security\HeaderUser;
use Doctrine\DBAL\Connection;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\User\InMemoryUser;


class Suc
{

    private readonly Session $session;
    private $user;
    private bool $mode_glucose = false; // Permettre la subtitution des valeurs de sessions (utile pour traitement différé)
    private array $valeurs = [];
    private readonly Preferenceur $preferenceur;

    function __construct(private readonly Security $security, Connection $db, private readonly Sac $sac)
    {
        $this->session = new Session();
        $this->preferenceur = new Preferenceur($this->sac, $db);
        $this->initSuc();
    }

    private function initSuc()
    {
        if (!$this->mode_glucose) {
            if (!$this->user) {
                $this->user = $this->security->getUser();
                if ($this->user !== null && !$this->isLoad()) {
                    $this->initPreference();
                    $vars = $this->get();
                    $vars['load'] = true;
                    $this->session->set('variable', $vars);
                }
            }
        }
        return true;
    }

    private function isLoad()
    {
        return $this->get('load');
    }

    /**
     * @param string $var
     * @return string|array|null
     */
    public function get($var = '')
    {
        $temp = [];
        $this->initSuc();
        if ($this->mode_glucose) {
            $temp = $this->valeurs['variable'] ?? [];
        } else {
            if ($this->session) {
                $temp = $this->session->get('variable');
            }
        }
        if ($var == '') {
            return $temp;
        }
        return tableauChemin($temp, $var);
    }

    function initPreference($id_utilisateur = null)
    {

        if (empty($id_utilisateur)) {
            $this->initSuc();
            $user = $this->user;
            $classname = $this->user::class;
            if (in_array($classname, [InMemoryUser::class, HeaderUser::class])) {
                $id_utilisateur = 0;
            } else {
                $id_utilisateur = (int)($user->getPrimaryKey());
            }
        }
        $tab = $this->preferenceur->lire_pref($id_utilisateur);
        if ($this->mode_glucose) {
            $this->valeurs['preference'] = ['generique' => $tab];
        } else {
            $this->session->set('preference', ['generique' => $tab]);
        }
        return true;
    }

    /**
     * @param string $var
     */
    public function set(string $var, mixed $value)
    {

        if ($var === '') {

        } else {
            if ($this->mode_glucose) {
                $temp_a = $this->valeurs['variable'] ?? [];
            } else {
                $temp_a = $this->session->get('variable');
            }
            $path_array = explode('.', $var);
            $temp = &$temp_a;
            foreach ($path_array as $key) {
                if (!isset($temp[$key])) {
                    $temp[$key] = [];
                }
                $temp = &$temp[$key];
            }
            $temp = $value;

            if ($this->mode_glucose) {
                $this->valeurs['variable'] = $temp_a;
            } else {
                $this->session->set('variable', $temp_a);
            }
        }

    }

    public function loadValeur($valeurs)
    {
        $this->mode_glucose = true;
        $this->valeurs = $valeurs;
    }

    /**
     * @return bool
     */
    public function isModeGlucose(): bool
    {
        return $this->mode_glucose;
    }

    /**
     * @param bool $mode_glucose
     */
    public function setModeGlucose(bool $mode_glucose): void
    {
        $this->mode_glucose = $mode_glucose;
    }

    function getEntitesNomDeLUtilisateur()
    {
        return array_intersect_key($this->sac->tab('entite'), array_flip($this->get('entite')));
    }

    function tab($objet)
    {

        $tab = $this->sac->tab($objet);
        $tab_id = $this->choice($objet);
        if ($tab_id) {
            return array_intersect_key($tab, array_flip($tab_id));
        } else {
            return $tab;
        }
    }

    function choice($objet)
    {
        $id_entite = $this->pref('en_cours.id_entite');
        return $this->get('choices.' . $id_entite . '.' . $this->sac->descr($objet . '.groupe') . '.' . $objet);
    }

    /**
     * pref
     * @param string $var
     * @param string $defaut
     * @return mixed
     */
    function pref($var = '', $defaut = '')
    {
        return $this->lire_preference($var, $defaut);
    }

    /**
     * lire_preference
     * @nom = la cle sous la forme importfichier.interactif
     * @defaut = la valeur de subsituion ;
     * @return null|string|array la valeur dude la cle de l'opérateur-société, l'operateur, générique,  ou la valeur envoyé par défaut
     */
    function lire_preference($nom, $defaut = '', $id_entite = 'generique')
    {
        $this->initSuc();
        if ($nom && $this->user) {

            $val_defaut = $this->sac->get('preference.' . $nom);
            if ($this->mode_glucose) {
                $pref_user = tableauChemin($this->valeurs['preference'], $id_entite . '.' . $nom);
            } else {
                $pref_user = tableauChemin($this->session->get('preference'), $id_entite . '.' . $nom);
            }
            if ($pref_user === null) {

                if ($val_defaut)
                    $val = $val_defaut;
                else
                    $val = $defaut;
            } else {
                if (is_array($pref_user)) {
                    $val = table_merge($val_defaut, $pref_user);
                } elseif ($pref_user === null) {
                    $val = $val_defaut;
                } else {

                    $val = $pref_user;
                }
            }
        } else {
            if ($this->user) {
                if ($this->mode_glucose) {
                    return $this->valeurs['preference'];
                } else {
                    return $this->session->get('preference');
                }

            } else {
                return null;
            }
        }
        if ($val === null) {
            return $defaut;
        }
        return $val;
    }

    function getEntitesDeLUtilisateur()
    {
        $tab_nom = [];
        $tab = array_intersect_key($this->sac->tab('entite'), array_flip($this->get('entite')));
        foreach ($tab as $id => $v) {
            $tab_nom[$v['nom']] = $id;
        }
        return $tab_nom;
    }

    /**
     * pref_exist
     * @param string $var
     * @return boolean
     */
    function pref_exist($var = '')
    {

        return ($this->sac->get('preference.' . $var) != null);
    }

    function ecrire_pref($chemin, $valeur, $id_utilisateur = null, $id_entite = null)
    {
        if ($id_utilisateur === null) {
            $id_utilisateur = $this->get('operateur.id');
        }
        $ok = $this->preferenceur->ecrire_pref($chemin, $valeur, $id_utilisateur, $id_entite);
        $tab_pref = $this->session->get('preference')['generique'];

        $tab_pref = $this->modifiePreferenceValeur($tab_pref, $chemin, $valeur);
        $this->session->set('preference', ['generique' => $tab_pref]);
        return $ok;
    }

    /**
     * est utiliser en enregistrement des modales utilisateur
     * et par ecrire_preference : remplissage dans la programmation des préférences par exemple
     *
     * @param $tab_pref array tableau du champs Valeur d'une l'intance de l'objet confiq à modifier
     * @param $chemin string la clef indique le point de départ des modifications.
     * @param $valeur array si saisie est valeur la branche est modifié par la valeur qui peut avoir des sous valeurs
     *                          ou un tableau
     *                                   chaque valeur remplace la branche de la ligne
     *                                   les branches non reseignées ne sont pas affectées.
     */

    function modifiePreferenceValeur($tab_pref, $chemin, $valeur)
    {
        $tab_chemin = explode('.', (string)$chemin);
        $clef_z = array_shift($tab_chemin);
        if (count($tab_chemin) > 0) {
            $tab_pref[$clef_z] = $this->modifiePreferenceValeur($tab_pref[$clef_z] ?? [], implode('.', $tab_chemin), $valeur);
        } else {
            if (empty($clef_z)) {
                return $valeur;
            } else {

                if (isset($tab_pref[$clef_z]) && is_array($tab_pref[$clef_z])) {
                    foreach ($tab_pref[$clef_z] as $k => $v) {
                        if (isset($valeur[$k])) {
                            $tab_pref[$clef_z][$k] = $valeur[$k];
                        }
                    }
                } else {
                    $tab_pref[$clef_z] = $valeur;
                }
            }
        }
        return $tab_pref;
    }

    function pref_merge($arr1, $arr2)
    {
        if (is_array($arr1) && is_array($arr2)) {
            foreach ($arr2 as $key => $value) {

                if (isset($arr1[$key]) && is_array($value))
                    $arr1[$key] = $this->pref_merge($arr1[$key], $arr2[$key]);
                else
                    $arr1[$key] = $value;
            }
        }
        return $arr1;
    }

    function initRestriction()
    {
        $restriction = [];
        return $restriction;
    }

    function getPreference($prefs, $nom = '')
    {

        if (!empty($nom)) {
            if (isset($prefs['generique']))
                return tableauChemin($prefs['generique'], $nom);
            else
                return null;
        }
        return $prefs;
    }
}