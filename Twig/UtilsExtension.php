<?php

namespace Declic3000\Pelican\Twig;


use Declic3000\Pelican\Component\Filtre\Filtre;
use Declic3000\Pelican\Service\Gendarme;
use Declic3000\Pelican\Service\Sac;
use Declic3000\Pelican\Service\Suc;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;


class UtilsExtension extends AbstractExtension
{

    protected $sac;

    protected $router;

    protected $translator;

    protected $twig;

    protected $suc;

    protected $csrf;

    protected $gendarme;

    protected $dispatcher;

    function __construct(Sac $sac, Suc $suc, UrlGeneratorInterface $router, TranslatorInterface $translator, Environment $twig, Gendarme $gendarme, CsrfTokenManagerInterface $csrf, EventDispatcherInterface $dispatcher)
    {
        $this->sac = $sac;
        $this->router = $router;
        $this->translator = $translator;
        $this->twig = $twig;
        $this->suc = $suc;
        $this->csrf = $csrf;
        $this->gendarme = $gendarme;
        $this->dispatcher = $dispatcher;
    }

    public function getFilters(): array
    {
        return [


            new TwigFilter('getPays', function ($id) {
                $pays = $this->sac->tab('pays');
                if (isset($pays[$id])) {
                    return $pays[$id];
                } else {
                    return '';
                }
            }),


            new TwigFilter('getNomUtilisateur', function ($id) {
                if ($id == 0)
                    return '';
                $operateur = $this->sac->tab('operateur.' . $id);
                if ($operateur)
                    return $operateur;
                else
                    return $id;
            }),
            new TwigFilter('array2htmltable', fn($table, $options) => array2htmltable($table, $options)),
            new TwigFilter('xedit', function ($value, $champs, $objet = null, $id = null, $type = 'text') {

                $objet ??= $this->sac->get('objet');
                $cle = $this->sac->descr($objet . '.cle');
                $id ??= $this->sac->get('id');
                $url = $this->router->generate($objet . '_xedit', [$cle => $id]);
                return '<a class="xeditable" data-name="' . $champs . '" data-type="' . $type . '" data-url="' . $url . '">' . $value . '</a>';
            }, ['is_safe' => ['html']]),


            new TwigFilter('transtab', function ($tab, $prefixe = '', $suffixe = '') {

                if (is_array($tab)) {
                    foreach ($tab as &$t) {
                        $t = $this->translator->trans($prefixe . $t . $suffixe);
                    }
                } else {
                    $tab = $this->translator->trans($prefixe . $tab . $suffixe);
                }
                return $tab;
            }),

        ];
    }

    public function getFunctions(): array
    {
        return [

            new TwigFunction('page_en_cours', fn($var = '') => $this->sac->get('page')),
            new TwigFunction('objet_en_cours', fn($var = '') => $this->sac->get('objet')),
            new TwigFunction('sac', fn($var = '') => $this->sac->get($var)),
            new TwigFunction('suc', fn($var = '') => $this->suc->get($var)),
            new TwigFunction('descr', fn($var = '') => $this->sac->descr($var)),
            new TwigFunction('tab', fn($var) => $this->sac->tab($var)),
            new TwigFunction('conf', fn($var) => $this->sac->conf($var)),
            new TwigFunction('pref', fn($var = '', $defaut = "") => $this->suc->pref($var, $defaut)),
            new TwigFunction('url_courante', fn() => $this->sac->get('url_courante')),
            new TwigFunction('mot', fn($nomcourt) => table_filtrer_valeur_premiere($this->sac->tab('mot'), 'nomcourt', $nomcourt)),
            new TwigFunction('fichier_twig', fn($type = '', $objet = '') => $this->sac->fichier_twig($type, $objet)),
            new TwigFunction('options_datatable', fn($objet, $datatable, $param = null) => $this->options_datatable($objet, $datatable, $param)),
            new TwigFunction('table_filtrer_valeur', fn($table, $cle, $value) => table_filtrer_valeur($table, $cle, $value)),
            new TwigFunction('documentation_lien', fn($page = '', $ancre = '') => $this->documentation_lien($page, $ancre), ['is_safe' => ['html']]),
            new TwigFunction('pref_exist', fn($pref) => $this->suc->pref_exist(str_replace('|', '.', $pref))),
            new TwigFunction('bouton', function ($action, $options = []) {
                $btn = $this->bouton($action, $options);
                if (!empty($btn)) {
                    return $this->twig->render('inclure/lien.html.twig', $btn);
                }
                return '';
            }, ['is_safe' => ['html']]),
            new TwigFunction('autoriser', fn($tab_profil, $id_entite = "") => $this->gendarme->autoriser($tab_profil), []),
            new TwigFunction('arbre', function ($code) {
                if (!is_array($code)) {
                    return $code;
                }
                return arbre($code);
            }),
            new TwigFunction('menu_page_droite', fn() => $this->menu_page_droite()),
            new TwigFunction('menu_page_gauche', fn() => $this->menu_page_gauche()),
            new TwigFunction('decode', function ($type, $code = null) {
                $type_autorise = [
                    'regle' => 'Regle',
                    'type_op' => 'TypeOp',
                    'etat' => 'Etat'
                ];
                if (isset($type_autorise[$type])) {
                    $nom_fonction = 'getListe' . $type_autorise[$type];
                    return $nom_fonction($code);
                }
                return $code;
            }),
            new TwigFunction('layout_cplt', fn() => $this->sac->get('ajax') ? '_zero' : ''),
            new TwigFunction('tache_a_executer', fn() => $this->sac->get('app.tache.robot') && ((integer)$this->sac->get('systeme.tache_time')) <= time())

        ];
    }


    function options_datatable($objet, $datatable, $param = null)
    {
        $options = [
            'action' => $param['action'] ?? true,
            'url' => $datatable['url'] ?? $this->router->generate($objet . '_index', $datatable['url_args'] ?? [])
        ];
        $cle = $this->sac->descr($objet . '.cle');
        $options_js = $datatable['options_js'];
        $args = [
            $cle => '--id--'
        ];
        $groupe = $this->sac->descr($objet . '.groupe');
        $options['cle'] = $cle;
        $bt = $options_js['bt'] ?? [];

        foreach ($bt as $nom_bt => &$b) {

            if (isset($b['route'])) {
                $args_route = $args;
                if (isset($b['route_args'])) {
                    foreach ($b['route_args'] as $k => $val) {
                        $args_route[$k] = $val;
                    }
                }
                if (isset($b['route_abs']) && $b['route_abs']) {
                    $b['url'] = $this->router->generate($b['route'], $args_route, UrlGeneratorInterface::ABSOLUTE_URL);
                    unset($b['route_abs']);
                } else {
                    $b['url'] = $this->router->generate($b['route'], $args_route);
                }
                unset($b['route']);
                unset($b['route_args']);
            }


            if (isset($b['role'])) {
                if (!$this->gendarme->isGrantedNiveau('ROLE_' . $b['role'])) {
                    unset($bt[$nom_bt]);
                } else {
                    unset($b['role']);
                }
            }
        }

        $url_show = "";
        if ($this->gendarme->autoriser(['OBJET_' . $groupe . '_VIS']) && !isset($options_js['inhibe_bt_show'])) {

            if (isset($datatable['options_js']['url'])) {
                if (!empty($datatable['options_js']['url'])) {
                    $url_show = $datatable['options_js']['url'];
                }
            } else {
                $url_show = $this->router->generate($objet . '_show', $args);
            }
            if (!empty($url_show)) {
                $bt['show'] = [
                    'id' => 'show',
                    'title' => 'Voir',
                    'icon' => 'fa fa-eye',
                    'url' => $url_show
                ];
            }

        }
        if ($this->gendarme->autoriser(['OBJET_' . $groupe . '_MOD']) && !isset($options_js['inhibe_bt_edit'])) {
            $bt['edit'] = [
                'id' => 'edit',
                'title' => 'Modification',
                'icon' => 'fa fa-pencil-alt',
                'couleur' => 'warning',
                'class' => 'modal-form',
                'redirect' => true,
                'url' => $this->router->generate($objet . '_edit', $args)
            ];
        }
        if ($this->gendarme->autoriser(['OBJET_' . $groupe . '_SUP']) && !isset($options_js['inhibe_bt_delete'])) {

            $bt['delete'] = [
                'id' => 'delete',
                'title' => 'Suppresion',
                'couleur' => 'danger',
                'icon' => 'fa fa-trash-alt',
                'attr' => 'data-confirm-supprimer="Supprimer cette ligne  ?"',
                'redirect' => true,
                'url' => $this->router->generate($objet . '_delete', $args)
            ];

        }
        $options['bt'] = $bt;

        if (!isset($param['redirection'])) {
            $options['redirection'] = urlencode((string)$this->sac->get('url_courante'));
        }
        if (isset($param['num_config_col']) && ((int)$param['num_config_col'] > 0)) {
            $options['num_config_col'] = $param['num_config_col'];
        }
        if (isset($datatable['options_ligne'])) {
            $options['options_ligne'] = $datatable['options_ligne'];
        }
        if ($datatable['colonnes']) {
            $options['colonnes'] = array_values($datatable['colonnes']);
            foreach ($options['colonnes'] as &$col) {
                $col['title'] = $this->translator->trans($col['title']);
            }
        }
        if ($datatable['colonnes_exclues']) {
            $options['colonnes_exclues'] = array_values($datatable['colonnes_exclues']);
        }
        if ($datatable['colonnes_set']) {
            $options['colonnes_set'] = $datatable['colonnes_set'];
        }
        if ($datatable['type_table']) {
            $options['type_table'] = $datatable['type_table'];
        }

        if ($datatable['tab_tri']['courant']) {
            $options['tri'] = $datatable['tab_tri']['courant'];
        }
        $filtres = Filtre::liste_filtre($datatable['filtres']);
        $filtres[] = 'query';
        $options['filtres'] = $filtres;
        if (isset($datatable['filtres_statiques'])) {
            $options['filtres_statiques'] = $datatable['filtres_statiques'];
        }

        if (isset($datatable['template'])) {
            $options['template'] = $datatable['template'];
        }

        return json_encode($options + $datatable['pref'] + $datatable['options_js']);
    }

    /**
     * documentation_lien
     *
     * @param string $page
     * @param string $ancre
     * @return string
     */
    function documentation_lien($page = '', $ancre = '')
    {

        $ancre = (!empty($ancre)) ? '#' . $ancre : '';
        return '/doc/fr/' . $page . $ancre;
    }

    /**
     * Rassemble et définit des propriétés nessécaire à la construction d'un bouton
     *
     * @param string $action
     *            permet de définir un ensemble de propriétés par défaut du bouton
     * @param array $options
     *            surcharge ou modifie les propriétés par défaut du bouton
     * @return array tableau de propriété d'un bouton ou d'un lien (généralement pour envoi vers la vue lien.html.twig )
     */
    function bouton(string $action, array $options = []): array
    {

        $objet = $options['objet'] ?? $this->sac->get('objet');
        $groupe = $this->sac->descr($objet . '.groupe');

        switch ($action) {
            case 'voir':
            case 'show':
            case 'imprimer':
                if (!$this->gendarme->autoriser(['OBJET_' . $groupe . '_VIS'])) {
                    return [];
                }
                break;
            case 'ajouter':
                if (!$this->gendarme->autoriser(['OBJET_' . $groupe . '_CRE'])) {
                    return [];
                }
                break;
            case 'taguer':
            case 'grouper':
            case 'lier':
            case 'delier':
            case 'taguer_theme':
            case 'taguer_mot':
            case 'modifier':
            case 'dupliquer':
            case 'changer_mdp':
            case 'retour_courrier':
                if (!$this->gendarme->autoriser(['OBJET_' . $groupe . '_MOD'])) {
                    return [];
                }
                break;
            case 'sortir':
            case 'fusion':
            case 'supprimer':
                if (!$this->gendarme->autoriser(['OBJET_' . $groupe . '_SUP'])) {
                    return [];
                }
                break;
        }

        $tab_action = ['modifier', 'supprimer', 'sortir', 'fusion', 'dupliquer'];
        // $id=($action=='ajouter' || ($action=='modifier' && isset($options['objet']) ))?'':sac('id');
        $id = $options['id'] ?? '';
        if (!isset($options['id']) && (in_array($action, $tab_action))) {
            $id = $this->sac->get('id');
        }
        $cle = $options['cle'] ?? $this->sac->descr($objet . '.cle');


        $args_url = [];

        if (!empty($id)) {
            $args_url = [
                $cle => $id
            ];

        }
        if ($action === 'ajouter' && isset($options['objet']) && $options['objet'] = !$this->sac->get('objet')) {
            $args_url = array_merge($args_url, [
                $this->sac->descr($this->sac->get('objet') . '.cle') => $this->sac->get('id')
            ]);
        }
        if (!isset($options['class_cplt'])) {
            $options['class_cplt'] = '';
        }
        if (isset($options['action'])) {
            $args_url = array_merge($args_url, ['action' => $options['action']]);
        }
        if (!isset($options['nom'])) {
            $options['nom'] = $objet . '_' . $action;
        }
        if (isset($options['args'])) {
            $args_url = array_merge($args_url, $options['args']);
        }
        if (isset($options['redirect'])) {
            $args_url['redirect'] = $options['redirect'];
        }
        if ($action === 'supprimer' && !isset($options['token'])) {
            $options['token'] = $this->csrf->getToken('delete-' . $objet . '-' . $id);
        }


        if (isset($options['token'])) {
            if (isset($options['attr'])) {
                $options['attr']['data-token'] = $options['token'];
            } else {
                $options['attr'] = ['data-token' => $options['token']];
            }
        }


        if (isset($options['page'])) {
            $page = $options['page'];
        } else {
            $objet = strtolower((string)$objet);
            $page = $objet . '_edit';
        }


        $options['action'] = $action;

        switch ($action) {
            case 'voir':
            case 'show':
                $options['nom'] = $objet;
                $page = $objet . '_show';
                break;
            case 'imprimer':
                $page = $options['page'] ?? 'impression_' . $objet;
                $args_url['id_' . $objet] = $id;
                break;
            case 'ajouter':
                $page = $options['page'] ?? $objet . '_new';
                $options['class_cplt'] .= ' modal-form';
                break;

            case 'modifier':
            case 'sortir':
                $options['class_cplt'] .= ' modal-form';
                break;

            case 'fusion':
                $page = $objet . '_fusion';
                $options['class_cplt'] .= ' modal-form';
                break;

            case 'changer_mdp':
                $page = 'utilisateur_mdp';
                break;
            case 'supprimer':
            case 'retour_courrier':

                $args_url['action'] = $action;
                $page = $options['page'] ?? $objet . '_delete';
                break;

            case 'dupliquer':
                $page = $objet . '_dupliquer';
                $options['class_cplt'] .= ' modal-form';
                break;

            case 'annuler':
                break;

            case 'grouper':
                $options['class_cplt'] .= ' modal-form';
                $args_url['idIndividu'] = $options['id_objet'] ?? $this->sac->get('id');
                $page = 'individu_groupe';
                break;
            case 'taguer':
                $options['class_cplt'] .= ' modal-form';
                $args_url['objet'] = $options['objet'] ?? $this->sac->get('objet');
                $args_url['id' . ucfirst((string)$objet)] = $options['id_objet'] ?? $this->sac->get('id');
                $page = $objet . '_mot';
                break;

            case 'taguer_theme':
                $options['class_cplt'] .= ' modal-form';
                $args_url['id' . $objet] = $options['id_' . $objet] ?? $this->sac->get('id');
                $page = 'theme_associer';
                break;
        }
        if (isset($options['page'])) {
            $page = $options['page'];
        }
        if (!isset($options['url'])) {
            $options['url'] = $this->router->generate($page, $args_url);
        }
        return $options;
    }

    /**
     * menu_page_droite
     *
     * @return array
     */
    function menu_page_droite()
    {
        $tab_fil = [];
        $type_page = $this->sac->get('type_page');
        $objet = $this->sac->get('objet');
        $cle = $this->sac->descr($objet . '.cle');
        $groupe = $this->sac->descr($objet . '.groupe');

        $nomcplt = $this->sac->get($objet . '.nomcplt');
        $id = $this->sac->get('id');
        if (!empty($objet)) {

            if ($this->gendarme->autoriser(['OBJET_' . $groupe . '_CRE'])) {

                if ($type_page !== 'edit' or $id) {

                    $tab_fil['ajouter'] = [
                        'action' => 'ajouter',
                        'bt_texte' => true,
                        'class_cplt' => 'modal-form',
                        'url' => $this->router->generate($objet . '_new')
                    ];
                    if ($type_page === 'show') {
                        $tab_fil['ajouter']['nom'] = $this->translator->trans($objet . '_ajouter');
                    }
                    $tp = $type_page;
                }
            }

            switch ($type_page) {
                case 'show':
                    if ($id) {
                        if ($this->gendarme->autoriser(['OBJET_' . $groupe . '_VIS'])) {
                            $tab_fil['x'] = [
                                'action' => 'modifier',
                                'url' => $this->router->generate($objet . '_edit', [
                                    $this->sac->descr($objet . '.cle') => $id
                                ]),
                                'class_cplt' => 'modal-form'
                            ];
                            if ($nomcplt) {
                                $tab_fil['x']['nom'] = $this->translator->trans($objet . '_modifier') . ' <span class="badge">' . $nomcplt . '</span>';
                            }
                            $tab_fil['x']['bt_texte'] = true;
                        }
                    }
                    break;
                case 'edit':

                    if ($id) {
                        $tab_fil['x']['action'] = 'show';
                        $tab_fil['x']['nom'] = $this->translator->trans('retour_fiche');
                        $tab_fil['x']['url'] = $this->router->generate($objet . '_show', [$cle => $id]);
                        if ($nomcplt) {
                            $tab_fil['x']['nom'] .= ' <span class="badge">' . $nomcplt . '</span>';
                        }
                    } else {
                        $tab_fil['x']['action'] = 'index';
                        $tab_fil['x']['nom'] = $this->translator->trans('retour_liste');
                        $tab_fil['x']['url'] = $this->router->generate($objet . '_index');
                    }
                    $tab_fil['x']['bt_texte'] = true;
                    break;
            }
            if ($type_page !== 'edit' or $id) {
                $redirect = $this->sac->get('url_courante');
                $tab_fil['preference'] = [
                    'action' => 'pref',
                    'url' => $this->router->generate('preference_edit', ['pref' => $objet, 'redirect' => $redirect])
                ];
            }
        }
        return $tab_fil;
    }

    /**
     * menu_page_gauche
     *
     * @return array
     */
    function menu_page_gauche()
    {
        $tab_fil = [];
        $page = $this->sac->get('page');
        $type_page = $this->sac->get('type_page');
        $objet = $this->sac->get('objet');
        $id = $this->sac->get('id');
        $action = $this->sac->get('action');
        if (empty($objet)) {
            $tab_fil['a'] = ['action' => $action, 'nom' => $page, 'url' => $page];
        } else {
            switch ($type_page) {
                case 'index':
                case 'multi':
                    $tab_fil['9'] = ['action' => $action, 'nom' => $this->translator->trans($objet . '_index')];
                    break;
                case 'show':
                case 'form':
                    $tab_fil['a'] = ['action' => $action, 'nom' => $this->translator->trans($objet . '_index'), 'url' => $this->router->generate($objet . '_index')];
                    if ($type_page === 'show') {
                        $tab_fil['b'] = ['action' => $action, 'nom' => $this->translator->trans($objet)];
                    } else {
                        $tab_fil['b'] = ['action' => $action, 'nom' => $this->translator->trans($objet . '_' . $action)];
                    }
                    if ($id) {
                        if ($this->sac->descr($objet . '.alias')) {
                            $objet = $this->sac->descr($objet . '.alias');
                        }

                        if ($this->sac->get($objet . '.nomcplt')) {
                            $tab_fil['b']['nom'] .= ' <span class="badge">' . $this->sac->get($objet . '.nomcplt') . '</span>';
                        }
                    }
                    break;
            }
        }
        ksort($tab_fil);
        return $tab_fil;
    }

    function routeExists($name)
    {
        return (null === $this->router->getRouteCollection()->get($name)) ? false : true;
    }


}



