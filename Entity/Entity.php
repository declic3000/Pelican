<?php

namespace Declic3000\Pelican\Entity;

use ArrayAccess;
use DateTime;
use Declic3000\Pelican\Serializer\ObjetNormalizer;
use Doctrine\ORM\Mapping as ORM;
use ReflectionClass;
use Stringable;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Serializer;

#[ORM\MappedSuperclass]
abstract class Entity implements ArrayAccess, Stringable
{

    public const COLONNES_EXCLUES_TRI = [];

    /**
     * Offset to retrieve
     * @link https://php.net/manual/en/arrayaccess.offsetget.php
     * @param mixed $offset <p>
     * The offset to retrieve.
     * </p>
     * @return mixed Can return all value types.
     */
    public function offsetGet(mixed $offset): mixed
    {
        if ($this->offsetExists($offset)) {
            $methode = 'get' . camelize($offset);
            return $this->$methode();
        }
        return null;
    }

    /**
     * @param string $offset
     * @return bool
     *
     */
    public function offsetExists($offset): bool
    {
        $methode = 'get' . camelize($offset);
        return method_exists($this, $methode);
    }

    /**
     * Offset to unset
     * @link https://php.net/manual/en/arrayaccess.offsetunset.php
     * @param mixed $offset <p>
     * The offset to unset.
     * </p>
     * @return void
     */
    public function offsetUnset(mixed $offset): void
    {
        $this->offsetSet($offset, null);
    }

    /**
     * Offset to set
     * @link https://php.net/manual/en/arrayaccess.offsetset.php
     * @param mixed $offset <p>
     * The offset to assign the value to.
     * </p>
     * @param mixed $value <p>
     * The value to set.
     * </p>
     * @return void
     */
    public function offsetSet(mixed $offset, mixed $value): void
    {
        if ($this->offsetExists($offset)) {
            $methode = 'set' . camelize($offset);
            $this->$methode($value);
        }
    }

    public function export()
    {
        $vars = $this->toArray();
        foreach ($vars as $k => $v) {
            if (is_object($v) && !is_a($k, DateTime::class)) {
                unset($vars[$k]);
            }
        }
        return $vars;
    }

    /**
     * @throws ExceptionInterface
     */
    public function toArray() :array
    {
        $serializer = new Serializer([new ObjetNormalizer()]);
        return $serializer->normalize($this,null);
    }

    public function toJson() :string
    {
        $serializer = new Serializer( [new ObjetNormalizer()],[(new JsonEncoder())]);
        return $serializer->serialize($this, 'json');
    }

    /**
     * in object2
     * @param $array
     */
    public function fromArray(array $array)
    {
        foreach ($array as $name => $value) {
            $methode = 'set' . camelize($name);
            if (method_exists($this, $methode)) {
                $this->$methode($value);
            }

        }
    }

    public function __toString(): string
    {
        if (method_exists($this, 'getNom')) {
            return (string)$this->getNom();
        }
        return $this->getPrimaryKey() . '';
    }

    public function getPrimaryKey()
    {
        $nom_class = (new ReflectionClass($this))->getShortName();
        $get = 'getId';
        if (!method_exists($this, $get)) {
            $get .= $nom_class;
        }
        return $this->$get();
    }


}
