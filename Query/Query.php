<?php

namespace Declic3000\Pelican\Query;

use Declic3000\Pelican\Service\Requete;
use Declic3000\Pelican\Service\Sac;
use Declic3000\Pelican\Service\Suc;
use Doctrine\DBAL\Connection;


class Query
{

    public static array $liaisons = [];
    protected $champs_recherche = [];
    protected $db;
    protected $requete;
    protected $sac;
    protected $suc;
    protected $nom_colonne_code; // colonne servant d'identifiant (ex : code département)
    protected $nom_colonne_nom; // libelle servant d'identifiant (ex : code département)

    public function __construct(protected $objet, Requete $requete, Connection $db, Sac $sac, Suc $suc)
    {

        $this->requete = $requete;
        $this->db = $db;
        $this->sac = $sac;
        $this->suc = $suc;

    }

    function getObjet()
    {
        return $this->objet;
    }

    function sanitizeFulltextQuery($chaine)
    {

        $keywordArray = explode(' ', (string)$chaine);
        $specialChar = ['*', '+', '-', ' ', '(', ')', '~', '@', '%', '<', '>'];
        $finalKeywords = [];
        foreach ($keywordArray as $keyword) {
            if (in_array($keyword, $specialChar) || empty($keyword) || strlen($keyword) < 3) {
                continue;
            }
            $prefixe = '+';
            $suffixe = '';
            if (in_array(substr($keyword, 0, 1), ['+', '-', '~', '*', '@', '<', '>'])) {
                if (in_array(substr($keyword, 0, 1), ['-'])) {
                    $prefixe = substr($keyword, 0, 1);
                }
                $keyword = substr($keyword, 1);
            }
            if (in_array(substr($keyword, -1), ['+', '-', '~', '*', '@', '<', '>'])) {
                if (in_array(substr($keyword, -1), ['*'])) {
                    $suffixe = substr($keyword, -1);
                }
                $keyword = substr($keyword, 0, -1);
            }
            $keyword = str_replace($specialChar, '.', $keyword);
            $finalKeywords[] = $prefixe . $keyword . $suffixe;
        }
        $finalString = implode(' ', $finalKeywords);
        return $finalString;
    }

    /**
     * @param array $params
     * @param array $options
     * @param string $preprefixe
     * @return array
     */
    function getWhere($params = [], $options = [], $preprefixe = '')
    {
        $pr = $preprefixe . $this->sac->descr($this->objet . '.nom_sql');
        $cle = $this->sac->descr($this->objet . '.cle_sql');
        $champs_recherche = empty($this->champs_recherche) ? [$this->sac->descr($this->objet . '.affichage')] : $this->champs_recherche;
        if (!empty($this->nom_colonne_code)) {
            $cle = $this->nom_colonne_code;
        }
        $tab_liaisons = [];
        $where = [];

        if (!empty($champs_recherche)) {
            $champs_recherche = $this->retravailleChampsRecherche($champs_recherche, $pr);
            // La recherche
            if (($search = $this->requete->ouArgs('search', $params))) {
                $recherche = trim((string)$search['value']);
                if (!empty($recherche)) {
                    $recherche = str_replace(['\'', '\\'], ['', '\\\\'], $recherche);
                    $where2 = '';
                    if ((int)$recherche > 0) {
                        $where2 = $cle . ' = \'' . $recherche . '\' OR ';
                    }
                    $where [] = ' ( ' . $where2 . implode(' like \'%' . $recherche . '%\' OR ', $champs_recherche) . ' like \'%' . $recherche . '%\')';
                }
            }
        }

        if ($id = $this->requete->ouArgs('id', $params)) {
            $where[] = $cle . ' = ' . $id;
        }

        if ($mots = $this->requete->ouArgs('mots', $params)) {
            if (!is_array($mots)) {
                $mots = [$mots];
            }
            $where [] = '(select count(mot.id_mot) from asso_mots_' . $this->objet . ' mot where mot.id_mot IN (' . implode(',',
                    $mots) . ') and mot.id_' . $this->objet . '=' . $pr . '.' . $cle . ' )=' . count($mots);
        }

        $restreindre_id = $this->requete->ouArgs('limitation_id', $params);

        if (!empty($restreindre_id)) {


            $restreindre_id = explode(',', (string)$restreindre_id);
            $where0 = ' ( ';
            foreach ($restreindre_id as $key => $id) {
                if ($key != 0) {
                    $where0 .= ' OR ';
                }
                $ids = explode('-', $id);
                if (count($ids) > 1) {
                    $where0 .= '(' . $pr . '.' . $cle . ' BETWEEN ' . $ids[0] . ' AND ' . $ids[1] . ')';
                } else {
                    $where0 .= $pr . '.' . $cle . ' =' . $ids[0];
                }

            }
            $where0 .= ')';
            $where[] = $where0;
        }

        $tab_colonnes = $this->sac->descr($this->objet . '.colonnes');
        $tab_colonnes_exclus = $this->sac->descr($this->objet . '.colonnes_query_exclus');
        if ($tab_colonnes) {
            foreach ($tab_colonnes as $colonne => $type) {

                if (in_array($colonne, $tab_colonnes_exclus)) {
                    continue;
                }
                if ($val = $this->requete->ouArgs($colonne, $params)) {
                    if (is_array($val)) {
                        foreach ($val as &$v) {
                            $v = $this->db->quote($v);
                        }
                        $where[] = $pr . '.' . $colonne . ' IN (' . implode(',', $val) . ')';
                    } else {
                        $where[] = $pr . '.' . $colonne . ' = ' . $this->db->quote($val);
                    }

                }
            }
        }
        return [$tab_liaisons, $where];
    }

    private function retravailleChampsRecherche(mixed $champs_recherche, $pr): array
    {
        $tab_liaison = $this->getLiaisons();
        $tab_result = [];
        foreach ($champs_recherche as &$cr) {
            if (strpos((string)$cr, '.') > -1) {
                [$ob, $c] = explode('.', (string)$cr);
                if (isset($tab_liaison[$ob])) {
                    $tab_result[] = $ob . '.' . $c;
                }
            } else {
                $tab_result[] = $pr . '.' . $cr;
            }
        }
        return $tab_result;
    }

    function getLiaisons($nom = null)
    {
        if ($nom) {
            return static::$liaisons[$nom];
        }
        return static::$liaisons;
    }

    function operateur_sql($operateur, $value)
    {

        $result = match ($operateur) {
            'equal', 'not_equal' => ' = ' . $value,
            'in', 'not_in' => ' IN (' . implode(',', $value) . ')',
            'is_null' => ' IS NULL',
            'is_not_null' => ' IS NOT NULL',
            default => '',
        };
        if (!empty($result) && str_starts_with((string)$operateur, 'not_')) {
            $result = ' NOT' . $result;
        }
        return $result;
    }

    function ope($param, $tab_operateur, $default = 'in')
    {
        return $tab_operateur[$param] ?? $default;
    }


}
