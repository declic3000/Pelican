<?php

namespace Declic3000\Pelican\Event;

use Symfony\Contracts\EventDispatcher\Event;

class PreferenceModifEvent extends Event
{
    public function __construct(private $returnValue)
    {
    }

    public function getPreference()
    {
        return $this->returnValue;
    }

    public function setPreference($returnValue)
    {
        $this->returnValue = $returnValue;
    }
}