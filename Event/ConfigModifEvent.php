<?php

namespace Declic3000\Pelican\Event;

use Symfony\Contracts\EventDispatcher\Event;

class ConfigModifEvent extends Event
{
    public function __construct(private $returnValue)
    {
    }

    public function getConfig()
    {
        return $this->returnValue;
    }

    public function setConfig($returnValue)
    {
        $this->returnValue = $returnValue;
    }
}