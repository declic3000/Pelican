<?php

/**
 * @param $array
 * @param $on
 * @param int $order
 * @return array
 */
function table_trier_par($array, $on, $order = SORT_ASC, int $flags = SORT_REGULAR)
{
    $new_array = [];
    $sortable_array = [];
    if (count($array) > 0) {
        foreach ($array as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $k2 => $v2) {
                    if ($k2 === $on) {
                        $sortable_array[$k] = $v2;
                    }
                }
            } else {
                $sortable_array[$k] = $v;
            }
        }

        switch ($order) {
            case SORT_ASC:
                asort($sortable_array, $flags);
                break;
            case SORT_DESC:
                arsort($sortable_array, $flags);
                break;
        }

        foreach ($sortable_array as $k => $v) {
            $new_array[$k] = $array[$k];
        }
    }

    return $new_array;
}


/**
 * @param array $table
 * @param string $cle
 * @param array|string $value
 * @return array|string
 */
function table_filtrer_valeur($table, $cle, $value)
{

    if (!is_array($table)) {
        return '';
    }
    if (!is_array($value)) {
        $value = [$value];
    }
    $tmp = [];
    $sub = (strpos('.', $cle) > -1);
    foreach ($table as $k => $v) {
        if ($sub) {
            $val = tableauChemin($v, $cle);
            if ($val === null) {
                continue;
            }
        } else {
            $val = $v[$cle];
        }
        if (in_array($val, $value)) {
            $tmp[$k] = $v;
        }
    }
    return $tmp;
}


/**
 * @param $table
 * @param $cle
 * @param int|float|DateTime $valeur_debut
 * @param int|float|DateTime $valeur_fin
 * @return array|string
 */
function table_filtrer_valeur_plage(array $table, $cle, $valeur_debut, $valeur_fin)
{


    $tmp = [];
    $sub = (strpos('.', (string)$cle) > -1);
    foreach ($table as $k => $v) {
        if ($sub) {
            $val = tableauChemin($v, $cle);
            if ($val === null) {
                continue;
            }
        } else {
            if (isset($v[$cle])) {
                $val = $v[$cle];
            } else {
                continue;
            }
        }
        if ($val >= $valeur_debut && $val <= $valeur_fin) {
            $tmp[$k] = $v;
        }
    }
    return $tmp;
}


/**
 * @param $table
 * @param $cle
 * @param $value
 * @return array|string
 */
function table_filtrer_valeur_existe($table, $cle, $value)
{

    if (!is_array($table)) {
        return '';
    }
    if (!is_array($value)) {
        $value = [$value];
    }
    $tmp = [];
    $sub = (strpos('.', (string)$cle) > -1);
    foreach ($table as $k => $v) {
        if ($sub) {
            $val = tableauChemin($v, $cle);
            if ($val === null) {
                continue;
            }
        } else {
            $val = $v[$cle];
        }

        if (count(array_intersect($val, $value)) > 0) {
            $tmp[$k] = $v;
        }
    }
    return $tmp;
}

/**
 * @param $table
 * @param $cle
 * @param $value
 * @return array|mixed|string
 */
function table_filtrer_valeur_premiere($table, $cle, $value)
{
    if (is_array($table)) {
        $res = table_filtrer_valeur($table, $cle, $value);
        return array_shift($res);
    }
    return [];
}


/**
 * @param $table
 * @param $cle
 * @param $value
 * @return array|mixed|string
 */
function table_exclure_valeur($table, $valeur_exclu)
{
    if (!is_array($valeur_exclu)) {
        $valeur_exclu = [$valeur_exclu];
    }
    foreach ($table as $id => $val) {
        if (in_array($val, $valeur_exclu)) {
            unset($table[$id]);
        }
    }
    return $table;
}


/**
 * @param $table
 * @param $cle
 * @param $value
 * @return array|mixed|string
 */
function table_exclure_cle_valeur($table, $cle = 'nom', $value = '')
{
    if (!is_array($value)) {
        $value = [$value];
    }
    foreach ($table as $id => $val) {
        if (in_array($val[$cle], $value)) {
            unset($table[$id]);
        }
    }
    return $table;
}


/**
 * @param $tab
 * @param string $champs_valeur
 * @param string $timestamp
 * @param string $champs_date
 * @return mixed
 */
function table_valeur_date($tab, $champs_valeur = 'montant', $timestamp = '', $champs_date = 'date_fin')
{
    if (!is_int($timestamp)) {
        $timestamp = time();
    }
    $result = $tab[0][$champs_valeur];
    foreach ($tab as $t) {
        if ($t[$champs_date] > $timestamp) {
            $result = $t[$champs_valeur];
        }
    }
    return $result;

}

/**
 * @param $tab
 * @param string $nom
 * @param array $filtre
 * @return array
 */
function table_simplifier($tab, $nom = 'nom', $filtre = [])
{
    $temp = [];
    if (is_array($tab)) {
        foreach ($tab as $k => $v) {
            $ok = true;
            foreach ($filtre as $kf => $vf) {
                if ($v[$kf] != $vf) $ok = false;
            }
            if (isset($v[$nom]) and $ok) {
                $temp[$k] = $v[$nom];
            }
        }
    }

    return $temp;
}


/**
 * @param $arr1
 * @param $arr2
 * @return array|mixed
 */
function table_merge($arr1, $arr2)
{
    if (is_array($arr1) && is_array($arr2)) {
        if (!empty($arr1)) {
            foreach ($arr1 as $key => $value) {
                if (isset($arr2[$key])) {
                    if (is_array($arr2[$key]) && is_array($value)) {
                        $arr1[$key] = table_merge($arr1[$key], $arr2[$key]);
                    } elseif (!is_array($arr2[$key]) && !is_array($value)) {
                        $arr1[$key] = $arr2[$key];
                    }
                } elseif (!is_array($value) && empty($arr2)) {
                    return [];
                }
            }
            foreach ($arr2 as $key => $value) {
                if (!isset($arr1[$key])) {
                    $arr1[$key] = $value;
                }
            }
        } else {
            $arr1 = $arr2;
        }
    } elseif (!is_array($arr1) && !is_array($arr2)) {
        $arr1 = $arr2;
    } elseif (!is_array($arr1) && is_array($arr2)) {
        $arr1 = $arr2;
    }
    return $arr1;
}

/**
 * @param $arr1
 * @param $arr2
 * @return array|mixed
 */
function table_merge2($arr1, $arr2)
{
    if (is_array($arr1) && is_array($arr2)) {
        if (!empty($arr1)) {
            foreach ($arr1 as $key => $value) {
                if (isset($arr2[$key])) {
                    if (isset($arr2[$key]['date']) && isset($arr2[$key]['timezone'])) {
                        $arr1[$key] = date_create($arr2[$key]['date'], new DateTimeZone($arr2[$key]['timezone']));
                    } elseif (is_array($arr2[$key]) && is_array($value)) {
                        $arr1[$key] = table_merge2($arr1[$key], $arr2[$key]);
                    } elseif (!is_array($arr2[$key]) && !is_array($value)) {
                        $arr1[$key] = $arr2[$key];
                    }
                }
            }
            foreach ($arr2 as $key => $value) {
                if (!isset($arr1[$key])) {
                    $arr1[$key] = $value;
                }
            }
        } else {
            $arr1 = $arr2;
        }
    } elseif (!is_array($arr1) && !is_array($arr2)) {
        $arr1 = $arr2;
    } elseif (!is_array($arr1) && is_array($arr2)) {
        $arr1 = $arr2;
    }
    return $arr1;
}


/**
 * @param $tab
 * @param string $nom
 * @return array
 */
function table_epurer($tab, $nom = 'nom')
{
    $temp = [];
    $nom = is_array($nom) ? $nom : [$nom];
    if (is_array($tab)) {
        foreach ($tab as $k => $v) {
            $ok = true;
            foreach ($nom as $n) {
                if (isset($v[$n]) and $ok) {
                    $temp[$k][$n] = $v[$n];
                }
            }
        }
    }

    return $temp;
}


/**
 * @param $tab
 * @param string[] $tab_champs
 * @param string $sep
 * @return array
 */
function table_simplifier_concat($tab, $tab_champs = ['code', 'nom'], $sep = ' - ')
{
    $temp = [];
    foreach ($tab as $k => $v) {

        $tt = [];
        foreach ($tab_champs as $champs) {
            if (isset($v[$champs])) {
                $tt[] = $v[$champs];
            }
        }
        $temp[$k] = implode($sep, $tt);
    }
    return $temp;
}


/**
 * @param $tab
 * @param $position
 * @param $tab_plus
 * @return array
 */
function table_inserer($tab, $position, $tab_plus, $preserve_key = false)
{
    $tab = array_merge(
        array_merge(array_slice($tab, 0, $position, $preserve_key),
            (is_array($tab_plus) ? $tab_plus : [$tab_plus])),
        array_slice($tab, $position, null, $preserve_key));
    if ($preserve_key) {
        return $tab;
    } else {
        return array_values($tab);
    }

}


/**
 * @param $tab
 * @param $indice
 * @return mixed
 */
function table_extraire_colonne($tab, $indice)
{
    $tab2 = $tab;
    foreach ($tab2 as &$t) {
        $t = $t[$indice];
    }
    return $tab2;
}

/**
 * @param array $tab
 * @param string $col_key
 * @param $col_valeur
 * @return array
 */
function table_colonne_cle_valeur($tab, string $col_key, $col_valeur)
{
    $tab2 = [];
    foreach ($tab as &$t) {

        if (is_array($col_valeur)) {
            $tab_temp = [];
            foreach ($col_valeur as $ind) {
                $tab_temp[] = $t[$ind];
            }
            $tab2[$t[$col_key]] = implode(' - ', $tab_temp);
        } else {
            $tab2[$t[$col_key]] = $t[$col_valeur];
        }

    }
    return $tab2;
}

/**
 * @param array $tab
 * @param string $indice_k
 * @param bool $effacer_col_indice
 * @return array
 */
function table_colonne_cle(array $tab, string $indice_k, bool $effacer_col_indice = false)
{
    $tab2 = [];
    foreach ($tab as &$t) {
        $t2 = $t;
        if ($effacer_col_indice) {
            unset($t2[$indice_k]);
        }
        $tab2[($t[$indice_k] ?? null)] = $t2;
    }
    return $tab2;
}

/**
 * @param array $tab
 * @param string $indice_k
 * @return array
 */
function table_colonne_cle_cumul(array $tab, string $indice_k)
{
    $tab2 = [];
    foreach ($tab as $t) {
        $tab2[$t[$indice_k]][] = $t;
    }
    return $tab2;
}


/**
 * @param array $tab
 * @param int $id_parent
 * @param int $niveau
 * @param string $cle
 * @return array
 */
function table_hierarchise(array $tab, int $id_parent = 0, int $niveau = 0, string $cle = 'parent_id')
{
    $result = [];
    $tab_id = table_filtrer_valeur($tab, $cle, $id_parent);
    foreach ($tab_id as $id => $elt)
        $result[$id] = table_hierarchise($tab, $id, $niveau + 1, $cle);
    return $result;
}


/**
 * @param array $arbre
 * @return array
 */
function table_en_arbre(array $tab, $cle = 'id', $cle_parent = 'parent_id', $cle_chlidren = 'children')
{
    $new = [];
    $resultat = [];
    foreach ($tab as $a) {
        $new[$a[$cle_parent]][] = $a;
    }
    $tab_racine = table_filtrer_valeur($tab, $cle_parent, 0);
    return createTree($new, $tab_racine, $cle);

}


function createTree(&$list, $parent, $cle = 'id', $cle_chlidren = 'children')
{
    $tree = [];
    foreach ($parent as $k => $l) {
        if (isset($list[$l[$cle]])) {
            $l[$cle_chlidren] = createTree($list, $list[$l[$cle]], $cle, $cle_chlidren);
        }
        $tree[$l[$cle]] = $l;
    }
    return $tree;
}


/**
 * @param array $arbre
 * @return array
 */
function table_en_arbre_id(array $tab, $cle = 'id', $cle_parent = 'parent_id')
{
    $new = [];
    foreach ($tab as $a) {
        $new[$a[$cle_parent]][$a[$cle]] = [];
    }
    $tab_racine = table_simplifier(table_filtrer_valeur($tab, $cle_parent, 0), $cle);
    return createTreeId($new, $tab_racine);
}

function createTreeId(&$list, $parent)
{
    $tree = [];
    foreach ($parent as $k => $l) {
        if (isset($list[$k])) {
            $l = createTreeId($list, $list[$k]);
            $tree[$k] = $l;
        } else {
            $tree[$k] = [];
        }
    }
    return $tree;
}


/**
 * @param array $arbre
 * @return array
 */
function arbre_linearise($arbre)
{
    $tab_ele = [];

    foreach ($arbre as $k => $a) {
        $tab_ele[] = $k;
        if (is_array($a)) {
            $tab_ele = array_merge($tab_ele, arbre_linearise($a));
        } else {
            if (!empty($a))
                $tab_ele[] = $a;
        }
    }
    return $tab_ele;
}


/**
 * @param array $tree
 * @param array $tab
 * @param int $niveau
 * @return array
 */
function arbre_linearise_niveau(array $tree, array $tab, int $niveau = 0, $cle = 'id')
{
    $result = [];
    if (is_array($tree)) {
        foreach ($tree as $id => $elt) {
            $tmp = $tab[$id];
            $tmp['niveau'] = $niveau;
            $result[] = $tmp;
            $result = array_merge($result, arbre_linearise_niveau($elt, $tab, $niveau + 1, $cle));
        }
    }

    $res = [];
    foreach ($result as $r) {
        $res[$r[$cle]] = $r;
    }
    return $res;
}


/**
 * @param array $tab
 * @param string $nom
 * @return array
 */
function table_valeur_cle(array $tab, string $nom = 'nom')
{
    $temp = [];
    foreach ($tab as $v) {
        $temp[$v[$nom]] = $v;
    }
    return $temp;
}

/**
 * @param array $tab
 * @param string $nom_cle_cle
 * @param string $nom_cle_valeur
 * @return array
 */
function table_cle(array $tab, string $nom_cle_cle = "cle", string $nom_cle_valeur = "valeur")
{
    $temp = [];
    foreach ($tab as $k => $v) {
        $temp[] = [$nom_cle_cle => $k, $nom_cle_valeur => $v];
    }
    return $temp;
}


/**
 * @param array $tab
 * @param string $nom_cle_indice
 * @param string $nom_cle_valeur
 * @param int $valeur_initiale
 * @return array
 */
function table_indice(array $tab, string $nom_cle_indice = "indice", string $nom_cle_valeur = "valeur", int $valeur_initiale = 1)
{
    $i = $valeur_initiale;
    foreach ($tab as &$v) {
        $v = [$nom_cle_indice => $i, $nom_cle_valeur => $v];
        $i++;
    }
    return $tab;
}


/**
 * @param array $tab
 * @param array $tab_key
 * @return array
 */
function table_combine(array $tab, array $tab_key)
{
    foreach ($tab as &$t) {
        $t = array_combine($tab_key, $t);
    }
    return $tab;
}


/**
 * @param array $tab_valeur_init
 * @param array $tab_correspondance
 * @param bool $preserve
 * @return array
 */

function table_correspondance(array $tab_valeur_init, array $tab_correspondance, bool $preserve = false)
{
    $tab = [];
    foreach ($tab_correspondance as $k => $c) {
        if (isset($tab_valeur_init[$k])) {
            $tab[$c] = $tab_valeur_init[$k];
            unset($tab_valeur_init[$k]);
        }
    }
    if ($preserve) {
        $tab = array_merge($tab, $tab_valeur_init);
    }
    return $tab;
}


/**
 * @param array $tab
 * @param string $nom
 * @param string $nouveau_nom
 * @return array
 */
function table_change_nom_colonne(array $tab, string $nom, string $nouveau_nom)
{

    foreach ($tab as &$t) {
        if (isset($t[$nom])) {
            $t[$nouveau_nom] = $t[$nom];
            unset($t[$nom]);
        }
    }
    return $tab;
}


/**
 * @param array $tab
 * @param string|array $col
 * @return array
 */
function table_regrouper(array $tab, $col)
{
    $tab_result = [];
    $col = is_array($col) ? $col : [$col];
    $c = array_shift($col);
    foreach ($tab as $k => $v) {
        $tab_result[$v[$c]][$k] = $v;
    }
    if (!empty($col)) {
        foreach ($tab_result as &$t) {
            $t = table_regrouper($t, $col);
        }
    }
    return $tab_result;
}


function table_ajout_modif_valeur($tab, $cle, $valeur)
{
    foreach ($tab as $k => &$v) {
        $v[$cle] = $valeur;
    }
    return $tab;
}