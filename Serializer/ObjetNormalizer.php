<?php

namespace Declic3000\Pelican\Serializer;


use Declic3000\Pelican\Entity\Entity;
use Symfony\Component\Serializer\Context\Normalizer\DateTimeNormalizerContextBuilder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AttributeLoader;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ObjetNormalizer implements NormalizerInterface
{
    public function __construct()
    {
    }

    public function normalize($objet, ?string $format = null, array $context = []): array
    {
        $classMetadataFactory = new ClassMetadataFactory(new AttributeLoader());
        $contextBuilder = (new DateTimeNormalizerContextBuilder())
            ->withFormat('Y-m-d H:i:s');
        $context = $contextBuilder->toArray();
        $context[AbstractObjectNormalizer::ENABLE_MAX_DEPTH]= true;
        $maxDepthHandler = fn(object $innerObject, object $outerObject, string $attributeName, ?string $format = null, array $context = []): string => $innerObject->getPrimaryKey();

        $defaultContext = [
            AbstractObjectNormalizer::MAX_DEPTH_HANDLER => $maxDepthHandler,
        ];
        $normalizer = new ObjectNormalizer($classMetadataFactory, null, null, null, null, null, $defaultContext);
        $serializer = new Serializer([new DateTimeNormalizer(),$normalizer]);
        return $serializer->normalize($objet, null,$context );
    }

    public function supportsNormalization($data, ?string $format = null, array $context = []): bool
    {
        return $data instanceof Entity;
    }

}